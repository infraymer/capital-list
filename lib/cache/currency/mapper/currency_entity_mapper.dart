import 'package:capital/cache/base/database.dart';
import 'package:capital/domain/currency/model/currency.dart';

class CurrencyEntityMapper {

  CurrencyEntity mapToCurrencyEntity(Currency currency) {
    return CurrencyEntity(currencyId: currency.id, name: currency.name);
  }
}