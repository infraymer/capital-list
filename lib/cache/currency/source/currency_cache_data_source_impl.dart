import 'package:capital/data/currency/source/currency_cache_data_source.dart';
import 'package:capital/domain/currency/model/currency.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CurrencyCacheDataSourceImpl extends CurrencyCacheDataSource {
  static const String _keyCurrencyId = 'currency_id';
  static const String _keyCurrencyName = 'currency_name';

  final Future<SharedPreferences> _preferences;

  CurrencyCacheDataSourceImpl(this._preferences);

  @override
  Future<Currency> getMainCurrency() async {
    final prefs = await _preferences;
    final currencyId = prefs.getString(_keyCurrencyId);
    final currencyName = prefs.getString(_keyCurrencyName);
    if (currencyId == null || currencyName == null) {
      return Currency(id: 'USD', name: 'United States Dollar');
    } else {
      return Currency(id: currencyId, name: currencyName);
    }
  }

  @override
  Future<void> setMainCurrency(Currency currency) async {
    final prefs = await _preferences;
    prefs.setString(_keyCurrencyId, currency.id);
    prefs.setString(_keyCurrencyName, currency.name);
  }
}