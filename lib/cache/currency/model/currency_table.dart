import 'package:moor/moor.dart';

@DataClassName('CurrencyEntity')
class CurrencyTable extends Table {
  TextColumn get currencyId => text()();
  TextColumn get name => text()();

  @override
  Set<Column> get primaryKey => {currencyId};

  @override
  String get tableName => 'currency';
}