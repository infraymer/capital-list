import 'package:capital/cache/base/database.dart';
import 'package:capital/cache/currency/model/currency_table.dart';
import 'package:moor/moor.dart';

part 'currency_dao.g.dart';

@UseDao(tables: [CurrencyTable])
class CurrencyDao extends DatabaseAccessor<AppDatabase> with _$CurrencyDaoMixin {
  final AppDatabase db;

  CurrencyDao(this.db) : super(db);

  Future<List<CurrencyEntity>> getAll() =>
      select(currencyTable).get();

  Future<CurrencyEntity> getById(String id) =>
      (select(currencyTable)..where((tbl) => tbl.currencyId.equals(id))).getSingle();

  Future<void> insertCurrency(CurrencyEntity currencyEntity) =>
      into(currencyTable).insert(currencyEntity, orReplace: true);
}