// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currency_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$CurrencyDaoMixin on DatabaseAccessor<AppDatabase> {
  $CurrencyTableTable get currencyTable => db.currencyTable;
}
