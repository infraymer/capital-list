// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'change_account_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$ChangeAccountDaoMixin on DatabaseAccessor<AppDatabase> {
  $ChangeAccountTableTable get changeAccountTable => db.changeAccountTable;
}
