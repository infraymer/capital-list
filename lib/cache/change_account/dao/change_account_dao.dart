import 'package:capital/cache/base/database.dart';
import 'package:capital/cache/change_account/model/change_account_table.dart';
import 'package:moor/moor.dart';

part "change_account_dao.g.dart";

@UseDao(tables: [ChangeAccountTable])
class ChangeAccountDao extends DatabaseAccessor<AppDatabase> with _$ChangeAccountDaoMixin {
  final AppDatabase db;

  ChangeAccountDao(this.db) : super(db);

  Future<List<ChangeAccountEntity>> getAll() =>
    select(changeAccountTable).get();

  Future<List<ChangeAccountEntity>> getMaxByCurrentDay(int min, int max) =>
      (select(changeAccountTable)
            ..orderBy([
              (u) => OrderingTerm(expression: u.time, mode: OrderingMode.asc)
            ])
            ..where(
              (tbl) => tbl.time.max().isSmallerThanValue(max),
            ))
          .get();

  Future<ChangeAccountEntity> getByDayAndByAccount(int day, int accountId) =>
      (select(changeAccountTable)
            ..orderBy([(u) => OrderingTerm(expression: u.time, mode: OrderingMode.desc)])
            ..where((tbl) => tbl.time.isSmallerThanValue(day),)
            ..where((tbl) => tbl.accountId.equals(accountId),)
            ..limit(1))
          .getSingle();

  Future<void> add(ChangeAccountEntity accountEntity) =>
    into(changeAccountTable).insert(accountEntity);
}
