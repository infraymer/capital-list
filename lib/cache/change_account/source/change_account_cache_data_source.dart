import 'package:capital/cache/change_account/dao/change_account_dao.dart';
import 'package:capital/cache/change_account/mapper/change_account_entity_mapper.dart';
import 'package:capital/data/change_account/source/change_account_cache_data_source.dart';
import 'package:capital/domain/change_account/model/change_account.dart';
import 'package:capital/domain/change_account/model/change_account_query.dart';

class ChangeAccountCacheDataSourceImpl extends ChangeAccountCacheDataSource {
  final ChangeAccountDao _changeAccountDao;

  ChangeAccountCacheDataSourceImpl(this._changeAccountDao);

  @override
  Future<void> addChangeAccount(ChangeAccount changeAccount) async {
    final entity = ChangeAccountEntityMapper.mapToChangeAccountEntity(changeAccount);
    await _changeAccountDao.add(entity);
  }

  @override
  Future<List<ChangeAccount>> getChangeAccountList(ChangeAccountQuery query) async {
    final changes = await _changeAccountDao.getAll();
    return changes.map((e) => ChangeAccountEntityMapper.mapToChangeAccount(e)).toList();
  }

  @override
  Future<ChangeAccount> getChangeAccountByDayAndByAccountId(int day, int accountId) async {
    final entity = await _changeAccountDao.getByDayAndByAccount(day, accountId);
    if (entity == null) return null;
    return ChangeAccountEntityMapper.mapToChangeAccount(entity);
  }
}