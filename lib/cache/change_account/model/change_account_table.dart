import 'package:capital/cache/account/model/account_table.dart';
import 'package:floor/floor.dart';
import 'package:moor/moor.dart';

@DataClassName('ChangeAccountEntity')
class ChangeAccountTable extends Table {

  TextColumn get changeAccountId => text()();
  RealColumn get value => real()();
  RealColumn get amountBefore => real()();
  RealColumn get amountAfter => real()();
  IntColumn get time => integer()();
  IntColumn get accountId => integer().nullable().customConstraint('NULL REFERENCES account(account_id)')();

  @override
  Set<Column> get primaryKey => {changeAccountId};

  @override
  String get tableName => 'change_account';
}