import 'package:capital/cache/base/database.dart';
import 'package:capital/domain/change_account/model/change_account.dart';

class ChangeAccountEntityMapper {

  static ChangeAccount mapToChangeAccount(ChangeAccountEntity entity) {
    return ChangeAccount(
      id: entity.changeAccountId,
      value: entity.value,
      amountBefore: entity.amountBefore,
      amountAfter: entity.amountAfter,
      time: entity.time,
      accountId: entity.accountId
    );
  }

  static ChangeAccountEntity mapToChangeAccountEntity(ChangeAccount data) {
    return ChangeAccountEntity(
        changeAccountId: data.id,
        value: data.value,
        amountBefore: data.amountBefore,
        amountAfter: data.amountAfter,
        time: data.time,
        accountId: data.accountId
    );
  }
}