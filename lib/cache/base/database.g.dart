// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class AccountEntity extends DataClass implements Insertable<AccountEntity> {
  final int accountId;
  final String name;
  final double amount;
  final int colorId;
  final String currencyId;
  AccountEntity(
      {@required this.accountId,
      @required this.name,
      @required this.amount,
      @required this.colorId,
      this.currencyId});
  factory AccountEntity.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final doubleType = db.typeSystem.forDartType<double>();
    return AccountEntity(
      accountId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}account_id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      amount:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}amount']),
      colorId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}color_id']),
      currencyId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}currency_id']),
    );
  }
  factory AccountEntity.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return AccountEntity(
      accountId: serializer.fromJson<int>(json['accountId']),
      name: serializer.fromJson<String>(json['name']),
      amount: serializer.fromJson<double>(json['amount']),
      colorId: serializer.fromJson<int>(json['colorId']),
      currencyId: serializer.fromJson<String>(json['currencyId']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'accountId': serializer.toJson<int>(accountId),
      'name': serializer.toJson<String>(name),
      'amount': serializer.toJson<double>(amount),
      'colorId': serializer.toJson<int>(colorId),
      'currencyId': serializer.toJson<String>(currencyId),
    };
  }

  @override
  AccountTableCompanion createCompanion(bool nullToAbsent) {
    return AccountTableCompanion(
      accountId: accountId == null && nullToAbsent
          ? const Value.absent()
          : Value(accountId),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      amount:
          amount == null && nullToAbsent ? const Value.absent() : Value(amount),
      colorId: colorId == null && nullToAbsent
          ? const Value.absent()
          : Value(colorId),
      currencyId: currencyId == null && nullToAbsent
          ? const Value.absent()
          : Value(currencyId),
    );
  }

  AccountEntity copyWith(
          {int accountId,
          String name,
          double amount,
          int colorId,
          String currencyId}) =>
      AccountEntity(
        accountId: accountId ?? this.accountId,
        name: name ?? this.name,
        amount: amount ?? this.amount,
        colorId: colorId ?? this.colorId,
        currencyId: currencyId ?? this.currencyId,
      );
  @override
  String toString() {
    return (StringBuffer('AccountEntity(')
          ..write('accountId: $accountId, ')
          ..write('name: $name, ')
          ..write('amount: $amount, ')
          ..write('colorId: $colorId, ')
          ..write('currencyId: $currencyId')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      accountId.hashCode,
      $mrjc(
          name.hashCode,
          $mrjc(
              amount.hashCode, $mrjc(colorId.hashCode, currencyId.hashCode)))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is AccountEntity &&
          other.accountId == this.accountId &&
          other.name == this.name &&
          other.amount == this.amount &&
          other.colorId == this.colorId &&
          other.currencyId == this.currencyId);
}

class AccountTableCompanion extends UpdateCompanion<AccountEntity> {
  final Value<int> accountId;
  final Value<String> name;
  final Value<double> amount;
  final Value<int> colorId;
  final Value<String> currencyId;
  const AccountTableCompanion({
    this.accountId = const Value.absent(),
    this.name = const Value.absent(),
    this.amount = const Value.absent(),
    this.colorId = const Value.absent(),
    this.currencyId = const Value.absent(),
  });
  AccountTableCompanion.insert({
    this.accountId = const Value.absent(),
    @required String name,
    @required double amount,
    @required int colorId,
    this.currencyId = const Value.absent(),
  })  : name = Value(name),
        amount = Value(amount),
        colorId = Value(colorId);
  AccountTableCompanion copyWith(
      {Value<int> accountId,
      Value<String> name,
      Value<double> amount,
      Value<int> colorId,
      Value<String> currencyId}) {
    return AccountTableCompanion(
      accountId: accountId ?? this.accountId,
      name: name ?? this.name,
      amount: amount ?? this.amount,
      colorId: colorId ?? this.colorId,
      currencyId: currencyId ?? this.currencyId,
    );
  }
}

class $AccountTableTable extends AccountTable
    with TableInfo<$AccountTableTable, AccountEntity> {
  final GeneratedDatabase _db;
  final String _alias;
  $AccountTableTable(this._db, [this._alias]);
  final VerificationMeta _accountIdMeta = const VerificationMeta('accountId');
  GeneratedIntColumn _accountId;
  @override
  GeneratedIntColumn get accountId => _accountId ??= _constructAccountId();
  GeneratedIntColumn _constructAccountId() {
    return GeneratedIntColumn('account_id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _amountMeta = const VerificationMeta('amount');
  GeneratedRealColumn _amount;
  @override
  GeneratedRealColumn get amount => _amount ??= _constructAmount();
  GeneratedRealColumn _constructAmount() {
    return GeneratedRealColumn(
      'amount',
      $tableName,
      false,
    );
  }

  final VerificationMeta _colorIdMeta = const VerificationMeta('colorId');
  GeneratedIntColumn _colorId;
  @override
  GeneratedIntColumn get colorId => _colorId ??= _constructColorId();
  GeneratedIntColumn _constructColorId() {
    return GeneratedIntColumn(
      'color_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _currencyIdMeta = const VerificationMeta('currencyId');
  GeneratedTextColumn _currencyId;
  @override
  GeneratedTextColumn get currencyId => _currencyId ??= _constructCurrencyId();
  GeneratedTextColumn _constructCurrencyId() {
    return GeneratedTextColumn('currency_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES currency(currency_id)');
  }

  @override
  List<GeneratedColumn> get $columns =>
      [accountId, name, amount, colorId, currencyId];
  @override
  $AccountTableTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'account';
  @override
  final String actualTableName = 'account';
  @override
  VerificationContext validateIntegrity(AccountTableCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.accountId.present) {
      context.handle(_accountIdMeta,
          accountId.isAcceptableValue(d.accountId.value, _accountIdMeta));
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (d.amount.present) {
      context.handle(
          _amountMeta, amount.isAcceptableValue(d.amount.value, _amountMeta));
    } else if (isInserting) {
      context.missing(_amountMeta);
    }
    if (d.colorId.present) {
      context.handle(_colorIdMeta,
          colorId.isAcceptableValue(d.colorId.value, _colorIdMeta));
    } else if (isInserting) {
      context.missing(_colorIdMeta);
    }
    if (d.currencyId.present) {
      context.handle(_currencyIdMeta,
          currencyId.isAcceptableValue(d.currencyId.value, _currencyIdMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {accountId};
  @override
  AccountEntity map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return AccountEntity.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(AccountTableCompanion d) {
    final map = <String, Variable>{};
    if (d.accountId.present) {
      map['account_id'] = Variable<int, IntType>(d.accountId.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.amount.present) {
      map['amount'] = Variable<double, RealType>(d.amount.value);
    }
    if (d.colorId.present) {
      map['color_id'] = Variable<int, IntType>(d.colorId.value);
    }
    if (d.currencyId.present) {
      map['currency_id'] = Variable<String, StringType>(d.currencyId.value);
    }
    return map;
  }

  @override
  $AccountTableTable createAlias(String alias) {
    return $AccountTableTable(_db, alias);
  }
}

class CurrencyEntity extends DataClass implements Insertable<CurrencyEntity> {
  final String currencyId;
  final String name;
  CurrencyEntity({@required this.currencyId, @required this.name});
  factory CurrencyEntity.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    return CurrencyEntity(
      currencyId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}currency_id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
    );
  }
  factory CurrencyEntity.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return CurrencyEntity(
      currencyId: serializer.fromJson<String>(json['currencyId']),
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'currencyId': serializer.toJson<String>(currencyId),
      'name': serializer.toJson<String>(name),
    };
  }

  @override
  CurrencyTableCompanion createCompanion(bool nullToAbsent) {
    return CurrencyTableCompanion(
      currencyId: currencyId == null && nullToAbsent
          ? const Value.absent()
          : Value(currencyId),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
    );
  }

  CurrencyEntity copyWith({String currencyId, String name}) => CurrencyEntity(
        currencyId: currencyId ?? this.currencyId,
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('CurrencyEntity(')
          ..write('currencyId: $currencyId, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(currencyId.hashCode, name.hashCode));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is CurrencyEntity &&
          other.currencyId == this.currencyId &&
          other.name == this.name);
}

class CurrencyTableCompanion extends UpdateCompanion<CurrencyEntity> {
  final Value<String> currencyId;
  final Value<String> name;
  const CurrencyTableCompanion({
    this.currencyId = const Value.absent(),
    this.name = const Value.absent(),
  });
  CurrencyTableCompanion.insert({
    @required String currencyId,
    @required String name,
  })  : currencyId = Value(currencyId),
        name = Value(name);
  CurrencyTableCompanion copyWith(
      {Value<String> currencyId, Value<String> name}) {
    return CurrencyTableCompanion(
      currencyId: currencyId ?? this.currencyId,
      name: name ?? this.name,
    );
  }
}

class $CurrencyTableTable extends CurrencyTable
    with TableInfo<$CurrencyTableTable, CurrencyEntity> {
  final GeneratedDatabase _db;
  final String _alias;
  $CurrencyTableTable(this._db, [this._alias]);
  final VerificationMeta _currencyIdMeta = const VerificationMeta('currencyId');
  GeneratedTextColumn _currencyId;
  @override
  GeneratedTextColumn get currencyId => _currencyId ??= _constructCurrencyId();
  GeneratedTextColumn _constructCurrencyId() {
    return GeneratedTextColumn(
      'currency_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [currencyId, name];
  @override
  $CurrencyTableTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'currency';
  @override
  final String actualTableName = 'currency';
  @override
  VerificationContext validateIntegrity(CurrencyTableCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.currencyId.present) {
      context.handle(_currencyIdMeta,
          currencyId.isAcceptableValue(d.currencyId.value, _currencyIdMeta));
    } else if (isInserting) {
      context.missing(_currencyIdMeta);
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {currencyId};
  @override
  CurrencyEntity map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return CurrencyEntity.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(CurrencyTableCompanion d) {
    final map = <String, Variable>{};
    if (d.currencyId.present) {
      map['currency_id'] = Variable<String, StringType>(d.currencyId.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    return map;
  }

  @override
  $CurrencyTableTable createAlias(String alias) {
    return $CurrencyTableTable(_db, alias);
  }
}

class ChangeAccountEntity extends DataClass
    implements Insertable<ChangeAccountEntity> {
  final String changeAccountId;
  final double value;
  final double amountBefore;
  final double amountAfter;
  final int time;
  final int accountId;
  ChangeAccountEntity(
      {@required this.changeAccountId,
      @required this.value,
      @required this.amountBefore,
      @required this.amountAfter,
      @required this.time,
      this.accountId});
  factory ChangeAccountEntity.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final doubleType = db.typeSystem.forDartType<double>();
    final intType = db.typeSystem.forDartType<int>();
    return ChangeAccountEntity(
      changeAccountId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}change_account_id']),
      value:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}value']),
      amountBefore: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}amount_before']),
      amountAfter: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}amount_after']),
      time: intType.mapFromDatabaseResponse(data['${effectivePrefix}time']),
      accountId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}account_id']),
    );
  }
  factory ChangeAccountEntity.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return ChangeAccountEntity(
      changeAccountId: serializer.fromJson<String>(json['changeAccountId']),
      value: serializer.fromJson<double>(json['value']),
      amountBefore: serializer.fromJson<double>(json['amountBefore']),
      amountAfter: serializer.fromJson<double>(json['amountAfter']),
      time: serializer.fromJson<int>(json['time']),
      accountId: serializer.fromJson<int>(json['accountId']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'changeAccountId': serializer.toJson<String>(changeAccountId),
      'value': serializer.toJson<double>(value),
      'amountBefore': serializer.toJson<double>(amountBefore),
      'amountAfter': serializer.toJson<double>(amountAfter),
      'time': serializer.toJson<int>(time),
      'accountId': serializer.toJson<int>(accountId),
    };
  }

  @override
  ChangeAccountTableCompanion createCompanion(bool nullToAbsent) {
    return ChangeAccountTableCompanion(
      changeAccountId: changeAccountId == null && nullToAbsent
          ? const Value.absent()
          : Value(changeAccountId),
      value:
          value == null && nullToAbsent ? const Value.absent() : Value(value),
      amountBefore: amountBefore == null && nullToAbsent
          ? const Value.absent()
          : Value(amountBefore),
      amountAfter: amountAfter == null && nullToAbsent
          ? const Value.absent()
          : Value(amountAfter),
      time: time == null && nullToAbsent ? const Value.absent() : Value(time),
      accountId: accountId == null && nullToAbsent
          ? const Value.absent()
          : Value(accountId),
    );
  }

  ChangeAccountEntity copyWith(
          {String changeAccountId,
          double value,
          double amountBefore,
          double amountAfter,
          int time,
          int accountId}) =>
      ChangeAccountEntity(
        changeAccountId: changeAccountId ?? this.changeAccountId,
        value: value ?? this.value,
        amountBefore: amountBefore ?? this.amountBefore,
        amountAfter: amountAfter ?? this.amountAfter,
        time: time ?? this.time,
        accountId: accountId ?? this.accountId,
      );
  @override
  String toString() {
    return (StringBuffer('ChangeAccountEntity(')
          ..write('changeAccountId: $changeAccountId, ')
          ..write('value: $value, ')
          ..write('amountBefore: $amountBefore, ')
          ..write('amountAfter: $amountAfter, ')
          ..write('time: $time, ')
          ..write('accountId: $accountId')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      changeAccountId.hashCode,
      $mrjc(
          value.hashCode,
          $mrjc(
              amountBefore.hashCode,
              $mrjc(amountAfter.hashCode,
                  $mrjc(time.hashCode, accountId.hashCode))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is ChangeAccountEntity &&
          other.changeAccountId == this.changeAccountId &&
          other.value == this.value &&
          other.amountBefore == this.amountBefore &&
          other.amountAfter == this.amountAfter &&
          other.time == this.time &&
          other.accountId == this.accountId);
}

class ChangeAccountTableCompanion extends UpdateCompanion<ChangeAccountEntity> {
  final Value<String> changeAccountId;
  final Value<double> value;
  final Value<double> amountBefore;
  final Value<double> amountAfter;
  final Value<int> time;
  final Value<int> accountId;
  const ChangeAccountTableCompanion({
    this.changeAccountId = const Value.absent(),
    this.value = const Value.absent(),
    this.amountBefore = const Value.absent(),
    this.amountAfter = const Value.absent(),
    this.time = const Value.absent(),
    this.accountId = const Value.absent(),
  });
  ChangeAccountTableCompanion.insert({
    @required String changeAccountId,
    @required double value,
    @required double amountBefore,
    @required double amountAfter,
    @required int time,
    this.accountId = const Value.absent(),
  })  : changeAccountId = Value(changeAccountId),
        value = Value(value),
        amountBefore = Value(amountBefore),
        amountAfter = Value(amountAfter),
        time = Value(time);
  ChangeAccountTableCompanion copyWith(
      {Value<String> changeAccountId,
      Value<double> value,
      Value<double> amountBefore,
      Value<double> amountAfter,
      Value<int> time,
      Value<int> accountId}) {
    return ChangeAccountTableCompanion(
      changeAccountId: changeAccountId ?? this.changeAccountId,
      value: value ?? this.value,
      amountBefore: amountBefore ?? this.amountBefore,
      amountAfter: amountAfter ?? this.amountAfter,
      time: time ?? this.time,
      accountId: accountId ?? this.accountId,
    );
  }
}

class $ChangeAccountTableTable extends ChangeAccountTable
    with TableInfo<$ChangeAccountTableTable, ChangeAccountEntity> {
  final GeneratedDatabase _db;
  final String _alias;
  $ChangeAccountTableTable(this._db, [this._alias]);
  final VerificationMeta _changeAccountIdMeta =
      const VerificationMeta('changeAccountId');
  GeneratedTextColumn _changeAccountId;
  @override
  GeneratedTextColumn get changeAccountId =>
      _changeAccountId ??= _constructChangeAccountId();
  GeneratedTextColumn _constructChangeAccountId() {
    return GeneratedTextColumn(
      'change_account_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _valueMeta = const VerificationMeta('value');
  GeneratedRealColumn _value;
  @override
  GeneratedRealColumn get value => _value ??= _constructValue();
  GeneratedRealColumn _constructValue() {
    return GeneratedRealColumn(
      'value',
      $tableName,
      false,
    );
  }

  final VerificationMeta _amountBeforeMeta =
      const VerificationMeta('amountBefore');
  GeneratedRealColumn _amountBefore;
  @override
  GeneratedRealColumn get amountBefore =>
      _amountBefore ??= _constructAmountBefore();
  GeneratedRealColumn _constructAmountBefore() {
    return GeneratedRealColumn(
      'amount_before',
      $tableName,
      false,
    );
  }

  final VerificationMeta _amountAfterMeta =
      const VerificationMeta('amountAfter');
  GeneratedRealColumn _amountAfter;
  @override
  GeneratedRealColumn get amountAfter =>
      _amountAfter ??= _constructAmountAfter();
  GeneratedRealColumn _constructAmountAfter() {
    return GeneratedRealColumn(
      'amount_after',
      $tableName,
      false,
    );
  }

  final VerificationMeta _timeMeta = const VerificationMeta('time');
  GeneratedIntColumn _time;
  @override
  GeneratedIntColumn get time => _time ??= _constructTime();
  GeneratedIntColumn _constructTime() {
    return GeneratedIntColumn(
      'time',
      $tableName,
      false,
    );
  }

  final VerificationMeta _accountIdMeta = const VerificationMeta('accountId');
  GeneratedIntColumn _accountId;
  @override
  GeneratedIntColumn get accountId => _accountId ??= _constructAccountId();
  GeneratedIntColumn _constructAccountId() {
    return GeneratedIntColumn('account_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES account(account_id)');
  }

  @override
  List<GeneratedColumn> get $columns =>
      [changeAccountId, value, amountBefore, amountAfter, time, accountId];
  @override
  $ChangeAccountTableTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'change_account';
  @override
  final String actualTableName = 'change_account';
  @override
  VerificationContext validateIntegrity(ChangeAccountTableCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.changeAccountId.present) {
      context.handle(
          _changeAccountIdMeta,
          changeAccountId.isAcceptableValue(
              d.changeAccountId.value, _changeAccountIdMeta));
    } else if (isInserting) {
      context.missing(_changeAccountIdMeta);
    }
    if (d.value.present) {
      context.handle(
          _valueMeta, value.isAcceptableValue(d.value.value, _valueMeta));
    } else if (isInserting) {
      context.missing(_valueMeta);
    }
    if (d.amountBefore.present) {
      context.handle(
          _amountBeforeMeta,
          amountBefore.isAcceptableValue(
              d.amountBefore.value, _amountBeforeMeta));
    } else if (isInserting) {
      context.missing(_amountBeforeMeta);
    }
    if (d.amountAfter.present) {
      context.handle(_amountAfterMeta,
          amountAfter.isAcceptableValue(d.amountAfter.value, _amountAfterMeta));
    } else if (isInserting) {
      context.missing(_amountAfterMeta);
    }
    if (d.time.present) {
      context.handle(
          _timeMeta, time.isAcceptableValue(d.time.value, _timeMeta));
    } else if (isInserting) {
      context.missing(_timeMeta);
    }
    if (d.accountId.present) {
      context.handle(_accountIdMeta,
          accountId.isAcceptableValue(d.accountId.value, _accountIdMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {changeAccountId};
  @override
  ChangeAccountEntity map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ChangeAccountEntity.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ChangeAccountTableCompanion d) {
    final map = <String, Variable>{};
    if (d.changeAccountId.present) {
      map['change_account_id'] =
          Variable<String, StringType>(d.changeAccountId.value);
    }
    if (d.value.present) {
      map['value'] = Variable<double, RealType>(d.value.value);
    }
    if (d.amountBefore.present) {
      map['amount_before'] = Variable<double, RealType>(d.amountBefore.value);
    }
    if (d.amountAfter.present) {
      map['amount_after'] = Variable<double, RealType>(d.amountAfter.value);
    }
    if (d.time.present) {
      map['time'] = Variable<int, IntType>(d.time.value);
    }
    if (d.accountId.present) {
      map['account_id'] = Variable<int, IntType>(d.accountId.value);
    }
    return map;
  }

  @override
  $ChangeAccountTableTable createAlias(String alias) {
    return $ChangeAccountTableTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $AccountTableTable _accountTable;
  $AccountTableTable get accountTable =>
      _accountTable ??= $AccountTableTable(this);
  $CurrencyTableTable _currencyTable;
  $CurrencyTableTable get currencyTable =>
      _currencyTable ??= $CurrencyTableTable(this);
  $ChangeAccountTableTable _changeAccountTable;
  $ChangeAccountTableTable get changeAccountTable =>
      _changeAccountTable ??= $ChangeAccountTableTable(this);
  AccountDao _accountDao;
  AccountDao get accountDao => _accountDao ??= AccountDao(this as AppDatabase);
  CurrencyDao _currencyDao;
  CurrencyDao get currencyDao =>
      _currencyDao ??= CurrencyDao(this as AppDatabase);
  ChangeAccountDao _changeAccountDao;
  ChangeAccountDao get changeAccountDao =>
      _changeAccountDao ??= ChangeAccountDao(this as AppDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [accountTable, currencyTable, changeAccountTable];
}
