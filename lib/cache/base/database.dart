import 'package:capital/cache/account/account_dao/account_dao.dart';
import 'package:capital/cache/account/model/account_table.dart';
import 'package:capital/cache/change_account/dao/change_account_dao.dart';
import 'package:capital/cache/change_account/model/change_account_table.dart';
import 'package:capital/cache/currency/dao/currency_dao.dart';
import 'package:capital/cache/currency/model/currency_table.dart';
import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'database.g.dart';

@UseMoor(
  tables: [
    AccountTable,
    CurrencyTable,
    ChangeAccountTable,
  ],
  daos: [
    AccountDao,
    CurrencyDao,
    ChangeAccountDao
  ],
)
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: 'db.sqlite', logStatements: true));

  @override
  int get schemaVersion => 1;
}