import 'package:capital/cache/account/account_dao/account_dao.dart';
import 'package:capital/cache/account/mapper/account_entity_mapper.dart';
import 'package:capital/cache/currency/dao/currency_dao.dart';
import 'package:capital/cache/currency/mapper/currency_entity_mapper.dart';
import 'package:capital/data/account/source/account_cache_data_source.dart';
import 'package:capital/domain/account/model/account.dart';

class AccountCacheDataSourceImpl extends AccountCacheDataSource {
  final AccountDao _accountDao;
  final CurrencyDao _currencyDao;
  final AccountEntityMapper _accountEntityMapper = AccountEntityMapper();
  final CurrencyEntityMapper _currencyEntityMapper = CurrencyEntityMapper();

  AccountCacheDataSourceImpl(this._accountDao, this._currencyDao);

  @override
  Future<void> saveAccount(Account account) async {
    final currencyEntity = _currencyEntityMapper.mapToCurrencyEntity(account.currency);
    final accountEntity = _accountEntityMapper.mapToAccountEntity(account);
    await _currencyDao.insertCurrency(currencyEntity);
    await _accountDao.addAccount(accountEntity);
  }

  @override
  Future<List<Account>> getAccounts() async {
    final accountEntities = await _accountDao.getAllAccounts();
    final accounts = <Account>[];
    for (final acc in accountEntities) {
      final currencyEntity = await _currencyDao.getById(acc.currencyId);
      final account = _accountEntityMapper.mapToAccount(acc, currencyEntity);
      accounts.add(account);
    }
    return accounts;
  }

  @override
  Future<void> deleteAccount(Account account) async {
    final entity = _accountEntityMapper.mapToAccountEntity(account);
    await _accountDao.deleteAccount(entity);
  }
}