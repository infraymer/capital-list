import 'package:capital/cache/account/model/account_table.dart';
import 'package:capital/cache/base/database.dart';
import 'package:moor/moor.dart';

part "account_dao.g.dart";

@UseDao(tables: [AccountTable])
class AccountDao extends DatabaseAccessor<AppDatabase> with _$AccountDaoMixin {
  final AppDatabase db;

  AccountDao(this.db) : super(db);

  Future<List<AccountEntity>> getAllAccounts() =>
      select(accountTable).get();

  Future<void> addAccount(AccountEntity accountEntity) =>
      into(accountTable).insert(accountEntity, orReplace: true);

  Future<void> deleteAccount(AccountEntity accountEntity) =>
      delete(accountTable).delete(accountEntity);
}
