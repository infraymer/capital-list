// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$AccountDaoMixin on DatabaseAccessor<AppDatabase> {
  $AccountTableTable get accountTable => db.accountTable;
}
