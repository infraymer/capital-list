import 'package:capital/cache/base/database.dart';
import 'package:capital/domain/account/model/account.dart';
import 'package:capital/domain/currency/model/currency.dart';

class AccountEntityMapper {
  AccountEntity mapToAccountEntity(Account account) {
    return AccountEntity(
      accountId: account.id,
      name: account.name,
      amount: account.amount,
      colorId: account.colorId,
      currencyId: account.currency.id,
    );
  }

  Account mapToAccount(AccountEntity accountEntity, CurrencyEntity currencyEntity) {
    return Account(
      id: accountEntity.accountId,
      name: accountEntity.name,
      amount: accountEntity.amount,
      colorId: accountEntity.colorId,
      currency: Currency(
        id: currencyEntity.currencyId,
        name: currencyEntity.name,
      ),
    );
  }
}