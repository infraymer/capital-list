import 'package:moor/moor.dart';

@DataClassName('AccountEntity')
class AccountTable extends Table {

  IntColumn get accountId => integer().autoIncrement()();
  TextColumn get name => text()();
  RealColumn get amount => real()();
  IntColumn get colorId => integer()();
  TextColumn get currencyId => text().nullable().customConstraint('NULL REFERENCES currency(currency_id)')();

  @override
  Set<Column> get primaryKey => {accountId};

  @override
  String get tableName => 'account';
}