import 'package:capital/view/app/capital_app.dart';
import 'package:capital/view/injection/init_injection.dart';
import 'package:flutter/material.dart';
import 'test_data.dart' as test_data;

void main() async {

  WidgetsFlutterBinding.ensureInitialized();

  await initInjectionModules();

//  await test_data.createAccounts();

  runApp(CapitalApp());
}
