import 'package:capital/domain/change_account/model/change_account.dart';
import 'package:capital/domain/change_account/model/change_account_query.dart';
import 'package:capital/domain/change_account/repository/change_account_repository.dart';

class ChangeAccountInteractor {
  final ChangeAccountRepository _changeAccountRepository;

  ChangeAccountInteractor(this._changeAccountRepository);

  Future<void> addChangeAccount(ChangeAccount changeAccount) =>
    _changeAccountRepository.addChangeAccount(changeAccount);

  Future<List<ChangeAccount>> getChangeAccountList(ChangeAccountQuery query) =>
      _changeAccountRepository.getChangeAccountList(query);
}