import 'package:flutter/foundation.dart';

class ChangeAccount {
  final String id;
  final double value;
  final double amountBefore;
  final double amountAfter;
  final int time;
  final int accountId;

//<editor-fold desc="Data Methods" defaultstate="collapsed">

  const ChangeAccount({
    @required this.id,
    @required this.value,
    @required this.amountBefore,
    @required this.amountAfter,
    @required this.time,
    @required this.accountId,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ChangeAccount &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          value == other.value &&
          amountBefore == other.amountBefore &&
          amountAfter == other.amountAfter &&
          time == other.time &&
          accountId == other.accountId);

  @override
  int get hashCode =>
      id.hashCode ^
      value.hashCode ^
      amountBefore.hashCode ^
      amountAfter.hashCode ^
      time.hashCode ^
      accountId.hashCode;

  @override
  String toString() {
    return 'ChangeAccount{' +
        ' id: $id,' +
        ' value: $value,' +
        ' amountBefore: $amountBefore,' +
        ' amountAfter: $amountAfter,' +
        ' time: $time,' +
        ' accountId: $accountId,' +
        '}';
  }

  ChangeAccount copyWith({
    String id,
    double value,
    double amountBefore,
    double amountAfter,
    int time,
    int accountId,
  }) {
    return new ChangeAccount(
      id: id ?? this.id,
      value: value ?? this.value,
      amountBefore: amountBefore ?? this.amountBefore,
      amountAfter: amountAfter ?? this.amountAfter,
      time: time ?? this.time,
      accountId: accountId ?? this.accountId,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'value': this.value,
      'amountBefore': this.amountBefore,
      'amountAfter': this.amountAfter,
      'time': this.time,
      'accountId': this.accountId,
    };
  }

  factory ChangeAccount.fromMap(Map<String, dynamic> map) {
    return new ChangeAccount(
      id: map['id'] as String,
      value: map['value'] as double,
      amountBefore: map['amountBefore'] as double,
      amountAfter: map['amountAfter'] as double,
      time: map['time'] as int,
      accountId: map['accountId'] as int,
    );
  }

//</editor-fold>
}