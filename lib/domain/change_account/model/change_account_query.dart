class ChangeAccountQuery {
  final int minTime;
  final int maxTime;

  ChangeAccountQuery({this.minTime, this.maxTime});
}