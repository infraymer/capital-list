import 'package:capital/domain/account/model/account.dart';

abstract class AccountRepository {

  Future<void> saveAccount(Account account);

  Future<void> deleteAccount(Account account);

  Future<List<Account>> getAccounts();
}