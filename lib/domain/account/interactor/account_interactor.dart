import 'package:capital/domain/account/model/account.dart';
import 'package:capital/domain/account/repository/account_repository.dart';

class AccountInteractor {
  final AccountRepository _accountRepository;

  AccountInteractor(this._accountRepository);

  Future<void> saveAccount(Account account) =>
      _accountRepository.saveAccount(account);

  Future<List<Account>> getAccounts() =>
      _accountRepository.getAccounts();

  Future<void> deleteAccount(Account account) =>
      _accountRepository.deleteAccount(account);
}