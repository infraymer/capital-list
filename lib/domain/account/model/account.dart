import 'package:capital/domain/currency/model/currency.dart';
import 'package:flutter/foundation.dart';

class Account {
  final int id;
  final String name;
  final double amount;
  final int colorId;
  final Currency currency;

//<editor-fold desc="Data Methods" defaultstate="collapsed">

  const Account({
    @required this.id,
    @required this.name,
    @required this.amount,
    @required this.colorId,
    @required this.currency,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Account &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name &&
          amount == other.amount &&
          colorId == other.colorId &&
          currency == other.currency);

  @override
  int get hashCode =>
      id.hashCode ^
      name.hashCode ^
      amount.hashCode ^
      colorId.hashCode ^
      currency.hashCode;

  @override
  String toString() {
    return 'Account{' +
        ' id: $id,' +
        ' name: $name,' +
        ' amount: $amount,' +
        ' colorId: $colorId,' +
        ' currency: $currency,' +
        '}';
  }

  Account copyWith({
    int id,
    String name,
    double amount,
    int colorId,
    Currency currency,
  }) {
    return new Account(
      id: id ?? this.id,
      name: name ?? this.name,
      amount: amount ?? this.amount,
      colorId: colorId ?? this.colorId,
      currency: currency ?? this.currency,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'name': this.name,
      'amount': this.amount,
      'colorId': this.colorId,
      'currency': this.currency,
    };
  }

  factory Account.fromMap(Map<String, dynamic> map) {
    return new Account(
      id: map['id'] as int,
      name: map['name'] as String,
      amount: map['amount'] as double,
      colorId: map['colorId'] as int,
      currency: map['currency'] as Currency,
    );
  }

//</editor-fold>
}
