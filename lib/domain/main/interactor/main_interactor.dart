import 'dart:math';

import 'package:capital/domain/account/repository/account_repository.dart';
import 'package:capital/domain/change_account/repository/change_account_repository.dart';
import 'package:capital/domain/currency/repository/currency_repository.dart';
import 'package:jiffy/jiffy.dart';

class MainInteractor {

  final CurrencyRepository _currencyRepository;
  final AccountRepository _accountRepository;
  final ChangeAccountRepository _changeAccountRepository;

  MainInteractor(this._currencyRepository,
      this._accountRepository,
      this._changeAccountRepository,);

  Future<double> getBalance() async {
    final accounts = await _accountRepository.getAccounts();
    final mainCurrency = await _currencyRepository.getMainCurrency();
    final currencyIds = accounts.map((e) => e.currency.id).toSet().toList();
    final rates = await _currencyRepository.getRates(
        mainCurrency.id, currencyIds);
    final ratesMap = rates.asMap().map((key, value) =>
        MapEntry(value.currencyId, value.value));
    final sum = accounts.fold<double>(
        0, (previousValue, element) => previousValue +
        (element.amount / ratesMap[element.currency.id]));
    return sum;
  }

  Future<List<double>> getChartWeekValues() async {
    final mainCurrency = await _currencyRepository.getMainCurrency();
    final accounts = await _accountRepository.getAccounts();
    final currencyIds = accounts.map((e) => e.currency.id).toSet().toList();
    final rates = await _currencyRepository.getRates(mainCurrency.id, currencyIds);
    final ratesMap = rates.asMap().map((key, value) => MapEntry(value.currencyId, value.value));

    final jiffy = Jiffy()
      ..startOf(Units.DAY)
      ..add(days: 1);

    final List<double> changes = [];

    for (var i = 0; i < 7; i++) {
      var sum = 0.0;
      for (final acc in accounts) {
        final change = await _changeAccountRepository
            .getChangeAccountByDayAndByAccountId(jiffy.valueOf(), acc.id);
        sum = sum + (change?.amountAfter ?? 0.0) / ratesMap[acc.currency.id];
      }
      changes.add(sum);
      jiffy.subtract(days: 1);
    }

    return changes.reversed.toList();
  }

  Future<List<double>> getChartMonthValues() async {
    final mainCurrency = await _currencyRepository.getMainCurrency();
    final accounts = await _accountRepository.getAccounts();
    final currencyIds = accounts.map((e) => e.currency.id).toSet().toList();
    final rates = await _currencyRepository.getRates(mainCurrency.id, currencyIds);
    final ratesMap = rates.asMap().map((key, value) => MapEntry(value.currencyId, value.value));

    final jiffy = Jiffy()
      ..startOf(Units.WEEK)
      ..add(weeks: 1);

    final List<double> changes = [];

    for (var i = 0; i < 31; i++) {
      var sum = 0.0;
      for (final acc in accounts) {
        final change = await _changeAccountRepository.getChangeAccountByDayAndByAccountId(jiffy.valueOf(), acc.id);
        sum = sum + (change?.amountAfter ?? 0.0) / ratesMap[acc.currency.id];
      }
      changes.add(sum);
      jiffy.subtract(weeks: 1);
    }

    return changes.reversed.toList();
  }

  Future<List<double>> getChartYearValues() async {
    final mainCurrency = await _currencyRepository.getMainCurrency();
    final accounts = await _accountRepository.getAccounts();
    final currencyIds = accounts.map((e) => e.currency.id).toSet().toList();
    final rates = await _currencyRepository.getRates(mainCurrency.id, currencyIds);
    final ratesMap = rates.asMap().map((key, value) => MapEntry(value.currencyId, value.value));

    final jiffy = Jiffy()
        ..startOf(Units.MONTH)
        ..add(months: 1);

    final List<double> changes = [];

    for (var i = 0; i < 12; i++) {
      var sum = 0.0;
      for (final acc in accounts) {
        final change = await _changeAccountRepository
            .getChangeAccountByDayAndByAccountId(jiffy.valueOf(), acc.id);
        sum = sum + (change?.amountAfter ?? 0.0) / ratesMap[acc.currency.id];
      }
      changes.add(sum);
      jiffy.subtract(months: 1);
    }

    return changes.reversed.toList();
  }

  Future<List<double>> getWeekData() async {
    return [3000, 1231, 5334, 3413, 9585, 2345, 13413];
  }

  Future<List<double>> getMonthData() async {
    return List.generate(31, (index) => Random().nextInt(100).toDouble());
  }

  Future<List<double>> getYearData() async {
    return List.generate(12, (index) => Random().nextInt(100).toDouble());
  }
}