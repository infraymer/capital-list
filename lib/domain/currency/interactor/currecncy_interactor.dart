import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/domain/currency/repository/currency_repository.dart';

class CurrencyInteractor {
  final CurrencyRepository _currencyRepository;

  CurrencyInteractor(this._currencyRepository);

  Future<List<Currency>> getCurrencyList() async =>
      _currencyRepository.getCurrencyList();

  Future<void> setMainCurrency(Currency currency) =>
      _currencyRepository.setMainCurrency(currency);

  Future<Currency> getMainCurrency() =>
      _currencyRepository.getMainCurrency();
}