import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/domain/currency/model/rate.dart';

abstract class CurrencyRepository {

  Future<List<Currency>> getCurrencyList();

  Future<void> setMainCurrency(Currency currency);

  Future<Currency> getMainCurrency();

  Future<List<Rate>> getRates(String baseCurrencyId, List<String> currencyIds);
}