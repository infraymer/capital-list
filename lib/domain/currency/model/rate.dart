import 'package:flutter/foundation.dart';

class Rate {
  final String currencyId;
  final double value;

//<editor-fold desc="Data Methods" defaultstate="collapsed">

  const Rate({
    @required this.currencyId,
    @required this.value,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Rate &&
          runtimeType == other.runtimeType &&
          currencyId == other.currencyId &&
          value == other.value);

  @override
  int get hashCode => currencyId.hashCode ^ value.hashCode;

  @override
  String toString() {
    return 'Rate{' + ' currencyId: $currencyId,' + ' value: $value,' + '}';
  }

  Rate copyWith({
    String currencyId,
    double value,
  }) {
    return new Rate(
      currencyId: currencyId ?? this.currencyId,
      value: value ?? this.value,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'currencyId': this.currencyId,
      'value': this.value,
    };
  }

  factory Rate.fromMap(Map<String, dynamic> map) {
    return new Rate(
      currencyId: map['currencyId'] as String,
      value: map['value'] as double,
    );
  }

//</editor-fold>
}