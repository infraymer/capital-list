import 'package:flutter/foundation.dart';

class Currency {
  final String id;
  final String name;

//<editor-fold desc="Data Methods" defaultstate="collapsed">

  const Currency({
    @required this.id,
    @required this.name,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Currency &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name);

  @override
  int get hashCode => id.hashCode ^ name.hashCode;

  @override
  String toString() {
    return 'Currency{' + ' id: $id,' + ' name: $name,' + '}';
  }

  Currency copyWith({
    String id,
    String name,
  }) {
    return new Currency(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'name': this.name,
    };
  }

  factory Currency.fromMap(Map<String, dynamic> map) {
    return new Currency(
      id: map['id'] as String,
      name: map['name'] as String,
    );
  }

//</editor-fold>
}