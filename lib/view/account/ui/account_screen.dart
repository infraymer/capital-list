import 'package:capital/base/extensions.dart';
import 'package:capital/domain/account/model/account.dart';
import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/presentation/account/account_bloc.dart';
import 'package:capital/presentation/account/account_event.dart';
import 'package:capital/view/base/ui/styles/colors.dart';
import 'package:capital/view/base/ui/styles/shapes.dart';
import 'package:capital/view/base/ui/styles/wg_icons.dart';
import 'package:capital/view/base/ui/widget/wg_button.dart';
import 'package:capital/view/base/ui/widget/wg_color_selector.dart';
import 'package:capital/view/base/ui/widget/wg_selector.dart';
import 'package:capital/view/base/ui/widget/wg_text_field.dart';
import 'package:capital/view/currency_selector/ui/currency_selector_screen.dart';
import 'package:capital/view/injection/init_injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AccountScreen extends StatefulWidget {
  static const ROUTE_NAME = '/account';
  final Account account;

  const AccountScreen({Key key, this.account}) : super(key: key);

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  final AccountBloc _bloc = getIt();
  final GlobalKey<NavigatorState> _navigatorKey = getIt();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _amountController = TextEditingController();
  Currency _currency;
  int _colorId;
  bool _isValid = false;

  @override
  void initState() {
    _nameController.addListener(_onNameChanged);
    _amountController.addListener(_onAmountChanged);
    if (widget.account != null) {
      _nameController.text = widget.account.name;
      _amountController.text = widget.account.amount.toString();
      _currency = widget.account.currency;
      _colorId = widget.account.colorId;
    }
    _validate();
    super.initState();
  }

  void _onNameChanged() {
    _validate();
  }

  void _onAmountChanged() {
    _validate();
  }

  void _onPlusMinusClicked() {
    if (_amountController.text.isEmpty) return;
    final value = int.parse(_amountController.text);
    _amountController.text = (-value).toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: ListView(
          children: <Widget>[
            _buildNameField(),
            SizedBox(height: 24),
            _buildAmountFiled(),
            SizedBox(height: 24),
            _buildCurrencyField(),
            SizedBox(height: 24),
            _buildColorSelector(),
            SizedBox(height: 24),
            _buildButtons(),
          ],
        ),
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text(widget.account == null ? 'New account' : 'Update account'),
      backgroundColor: WgColors.mainColor,
      shape: WgShapes.appbarShape,
      actions: <Widget>[
        if (widget.account != null)
          IconButton(
            icon: Icon(Icons.delete_outline),
            onPressed: _onDeleteClicked,
          )
      ],
    );
  }

  Widget _buildNameField() {
    return WgTextField(
      labelText: 'Add a name',
      hintText: 'E.g., dividends',
      controller: _nameController,
    );
  }

  Widget _buildAmountFiled() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: WgTextField(
            labelText: 'Amount',
            hintText: '+',
            controller: _amountController,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              // WhitelistingTextInputFormatter.digitsOnly
            ], // Only numbers can be entered
          ),
        ),
        IconButton(
          icon: Icon(WgFlutterApp.plus_minus),
          onPressed: _onPlusMinusClicked,
        )
      ],
    );
  }

  Widget _buildCurrencyField() {
    return WgSelectorField(
      labelText: 'Currency',
      hint: 'Select',
      value: _currency?.id,
      onTap: () async {
        final data = await Navigator.of(context).pushNamed(CurrencySelectorScreen.ROUTE_NAME, arguments: _currency);
        if (data == null) return;
        _currency = data.cast<Currency>();
        _validate();
      },
    );
  }

  Widget _buildColorSelector() {
    return WgColorSelector(
      selectedIndex: _colorId ?? -1,
      onTap: (colorId) {
        _colorId = colorId;
        _validate();
      },
    );
  }

  Widget _buildButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        WgMainButton(
          text: 'CANCEL',
          textColor: WgColors.mainColor,
          elevation: 0,
          onPressed: () => Navigator.pop(context),
        ),
        SizedBox(width: 4),
        WgMainButton(
          text: 'SAVE',
          backgroundColor: WgColors.mainColor,
          onPressed: _isValid ? _onSaveClicked : null,
        ),
      ],
    );
  }

  void _validate() {
    _isValid = _nameController.text.isNotEmpty &&
        _amountController.text.isNotEmpty &&
        _colorId != null &&
        _currency != null;
    setState(() {});
  }

  void _onSaveClicked() {
    final data = Account(
      id: widget.account?.id ?? DateTime.now().millisecondsSinceEpoch,
      name: _nameController.text,
      amount: double.parse(_amountController.text),
      colorId: _colorId,
      currency: _currency,
    );
    _bloc.add(SaveAccountEvent(data, widget.account?.amount ?? 0));
    _navigatorKey.currentState.pop();
  }

  void _onDeleteClicked() {
    _bloc.add(DeleteAccountEvent(widget.account));
    _navigatorKey.currentState.pop();
  }
}
