import 'package:capital/view/base/ui/styles/colors.dart';
import 'package:flutter/cupertino.dart';

class WgTextStyles {
  static const TextStyle labelTextFieldStyle = const TextStyle(
    fontWeight: FontWeight.bold,
    color: WgColors.mainColor,
    fontSize: 14
  );
}