import 'package:flutter/material.dart';

class WgShapes {
  static const appbarShape = RoundedRectangleBorder(
    borderRadius: BorderRadius.only(
      bottomLeft: Radius.circular(40),
      bottomRight: Radius.circular(40),
    ),
  );
}