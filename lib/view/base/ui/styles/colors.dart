import 'package:flutter/cupertino.dart';

class WgColors {
  static const Color mainColor = Color(0xFF10101E);
  static const Color hintTextFieldColor = Color(0x5010101E);
  static const Color appbarColor = mainColor;
  static const Color divider = Color(0x0F10101E);
  static const Color disableButtonColor = Color(0x407F669E);

  static const List<Color> accountColors = [
    Color(0xFF1CF1FE),
    Color(0xFFFF2264),
    Color(0xFFFFCA06),
    Color(0xFF8A1AFB),
    Color(0xFF1E1AF9),
    Color(0xFF43C822),
    Color(0xFFFF6E1D),
  ];
}