import 'package:capital/view/base/ui/styles/colors.dart';
import 'package:capital/view/base/ui/styles/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class WgTextField extends StatelessWidget {
  final String labelText;
  final String hintText;
  final int maxLength;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final List<TextInputFormatter> inputFormatters;
  final bool obscureText;
  final int minLines;
  final int maxLines;
  final bool enabled;
  final Widget suffixIcon;
  final Widget prefixIcon;
  final GestureTapCallback onTap;
  final ValueChanged<String> onChanged;

  const WgTextField(
      {Key key,
      this.hintText,
      this.controller,
      this.inputFormatters,
      this.maxLength,
      this.keyboardType,
      this.obscureText = false,
      this.minLines = 1,
      this.maxLines = 1,
      this.enabled,
      this.suffixIcon,
      this.prefixIcon,
      this.onTap,
      this.labelText,
      this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          labelText,
          style: WgTextStyles.labelTextFieldStyle,
        ),
//        SizedBox(height: 10),
        TextField(
          enabled: enabled,
          maxLength: maxLength,
          controller: controller,
          keyboardType: keyboardType,
          inputFormatters: inputFormatters,
          obscureText: obscureText,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w500,
          ),
          minLines: minLines,
          onTap: onTap,
          maxLines: maxLines,
          onChanged: onChanged,
          decoration: InputDecoration(
            hintStyle: TextStyle(
              color: WgColors.hintTextFieldColor,
              fontSize: 12,
              fontWeight: FontWeight.w500,
            ),
            border: InputBorder.none,
            suffixIcon: suffixIcon,
            prefixIcon: prefixIcon,
            hintText: hintText,
            contentPadding: EdgeInsets.all(0),
          ),
        ),
//        SizedBox(height: 8),
        Container(
          width: double.infinity,
          height: 1,
          color: WgColors.mainColor,
        )
      ],
    );
  }
}
