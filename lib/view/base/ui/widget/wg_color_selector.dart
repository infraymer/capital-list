import 'package:capital/view/base/ui/styles/colors.dart';
import 'package:capital/view/base/ui/styles/text_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WgColorSelector extends StatefulWidget {

  final Function(int colorId) onTap;
  final int selectedIndex;

  const WgColorSelector({Key key, this.onTap, this.selectedIndex = -1}) : super(key: key);

  @override
  _WgColorSelectorState createState() => _WgColorSelectorState(selectedIndex);
}

class _WgColorSelectorState extends State<WgColorSelector> {
  final _colors = WgColors.accountColors;

  int _selectedIndex;

  _WgColorSelectorState(this._selectedIndex);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Choose color',
          style: WgTextStyles.labelTextFieldStyle
        ),
        SizedBox(height: 12),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            for (final color in _colors)
              CircleButton(
                color: color,
                isSelected: _isSelected(color),
                onTap: () {
                  _selectedIndex = _colors.indexOf(color);
                  setState(() {});
                  widget.onTap(_selectedIndex);
                },
              )
          ],
        ),
      ],
    );
  }

  bool _isSelected(Color color) {
    try {
      if (_selectedIndex < 0) return true;
      return _selectedIndex == _colors.indexOf(color);
    } catch(e) {
      return true;
    }
  }
}

class CircleButton extends StatelessWidget {
  final Color color;
  final Function onTap;
  final bool isSelected;

  const CircleButton(
      {Key key, @required this.color, this.onTap, this.isSelected = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 35,
      height: 35,
      child: Opacity(
        opacity: isSelected ? 1 : 0.3,
        child: Material(
          shape: CircleBorder(),
          color: color,
          child: InkWell(
            borderRadius: BorderRadius.circular(60),
            onTap: onTap,
          ),
        ),
      ),
    );
  }
}
