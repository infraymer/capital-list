import 'package:flutter/material.dart';

class WgMainButton extends StatelessWidget {
  final Function onPressed;
  final String text;
  final Color backgroundColor;
  final Color textColor;
  final double elevation;

  const WgMainButton({
    Key key,
    this.onPressed,
    this.text,
    this.backgroundColor,
    this.textColor,
    this.elevation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: backgroundColor,
      elevation: elevation ?? 4,
      onPressed: onPressed,
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(5),
      ),
      disabledColor:
          backgroundColor == null ? Colors.transparent : Colors.black12,
      disabledTextColor:
          backgroundColor == null ? Colors.black45 : Colors.white,
      textColor: textColor ?? Colors.white,
      child: Text(
        text,
        style: TextStyle(
//            color: textColor ?? Colors.white,
            fontSize: 12,
            fontWeight: FontWeight.w600),
      ),
    );
  }
}
