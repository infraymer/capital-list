import 'package:capital/view/base/ui/styles/colors.dart';
import 'package:capital/view/base/ui/styles/text_styles.dart';
import 'package:flutter/material.dart';

class WgSelectorField extends StatefulWidget {
  final String labelText;
  final String hint;
  final String value;
  final Function onTap;

  const WgSelectorField({
    Key key,
    @required
    this.labelText,
    this.hint,
    this.onTap,
    this.value,
  }) : super(key: key);

  @override
  _WgSelectorFieldState createState() => _WgSelectorFieldState();
}

class _WgSelectorFieldState extends State<WgSelectorField> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            widget.labelText,
            style: WgTextStyles.labelTextFieldStyle,
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Text(
                  widget.value ?? widget.hint,
                  style: TextStyle(
                    color: widget.value == null ? WgColors.hintTextFieldColor : WgColors.mainColor,
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Icon(
                Icons.keyboard_arrow_right,
                color: WgColors.mainColor,
                size: 16,
              )
            ],
          ),
          SizedBox(height: 8),
          Container(
            width: double.infinity,
            height: 1,
            color: WgColors.mainColor,
          )
        ],
      ),
    );
  }
}
