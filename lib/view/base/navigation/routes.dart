import 'package:capital/domain/account/model/account.dart';
import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/view/account/ui/account_screen.dart';
import 'package:capital/view/currency_selector/ui/currency_selector_screen.dart';
import 'package:capital/view/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

final appRoutes = {
  MainScreen.ROUTE_NAME: (ctx) => MainScreen(),
  AccountScreen.ROUTE_NAME: (ctx) => AccountScreen(),
  CurrencySelectorScreen.ROUTE_NAME: (ctx) => CurrencySelectorScreen(),
};

Route<dynamic> onGenerateRoute(RouteSettings settings) {
  Widget screen;

  if (settings.name == AccountScreen.ROUTE_NAME) {
    final Account data = settings.arguments;
    screen = AccountScreen(account: data);
  }

  if (settings.name == MainScreen.ROUTE_NAME) {
    screen = MainScreen();
  }

  if (settings.name == CurrencySelectorScreen.ROUTE_NAME) {
    final Currency data = settings.arguments;
    screen = CurrencySelectorScreen(currency: data);
  }

  return MaterialPageRoute(builder: (ctx) => screen);
}


