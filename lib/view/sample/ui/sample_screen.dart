import 'package:capital/base/extensions.dart';
import 'package:capital/presentation/sample/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SampleScreen extends StatefulWidget {
  @override
  _SampleScreenState createState() => _SampleScreenState();
}

class _SampleScreenState extends State<SampleScreen> {
  final SimpleBloc _bloc = SimpleBloc();

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Workingeeks'),
      ),
      body: BlocBuilder<SimpleBloc, SimpleState>(
          bloc: _bloc,
          builder: (BuildContext ctx, SimpleState state) {
            final count = state.cast<DataSimpleState>()?.count?.toString() ?? '';

            return Container(
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  state is LoadingSimpleState ? _progress() : _value(count),
                  SizedBox(height: 32),
                  RaisedButton(
                    child: Text('Increment'),
                    onPressed:
                        state is DataSimpleState ? _onIncrementClicked : null,
                  )
                ],
              ),
            );
          }),
    );
  }

  void _onIncrementClicked() {
    _bloc.add(IncrementSimpleEvent());
  }

  Widget _value(String text) {
    return Text(
      text,
      style: Theme.of(context).textTheme.title.apply(
            color: Theme.of(context).accentColor,
          ),
    );
  }

  Widget _progress() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}
