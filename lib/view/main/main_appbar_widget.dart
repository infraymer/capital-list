import 'package:capital/base/extensions.dart';
import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/presentation/main/bloc.dart';
import 'package:capital/presentation/main_appbar/bloc.dart';
import 'package:capital/view/base/ui/styles/wg_icons.dart';
import 'package:capital/view/currency_selector/ui/currency_selector_screen.dart';
import 'package:capital/view/injection/init_injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WgMainAppBar extends StatelessWidget {

  final GlobalKey<NavigatorState> _navigator = getIt();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainAppbarBloc, MainAppbarState>(
      builder: (context, state) {

        final DataMainAppbarState dataMainState = state.cast<DataMainAppbarState>();

        return SliverAppBar(
          iconTheme: Theme.of(context).iconTheme.copyWith(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 16,
          expandedHeight: 115,
          pinned: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(40),
              bottomRight: Radius.circular(40),
            ),
          ),
          leading: IconButton(
            icon: Icon(
              WgFlutterApp.report,
            ),
            onPressed: () {},
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                WgFlutterApp.currency,
              ),
              onPressed: () async {
                if (dataMainState == null) return;
                final data = await _navigator.currentState.pushNamed(CurrencySelectorScreen.ROUTE_NAME, arguments: dataMainState.currency);
                if (data is! Currency) return;
                BlocProvider.of<MainAppbarBloc>(context).add(CurrencySelectedMainAppbarEvent(data));
              },
            )
          ],
          flexibleSpace: FlexibleSpaceBar(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  transform: Matrix4.identity()..translate(0.0, -3),
                  child: Text(
                    dataMainState?.currency != null
                        ? dataMainState.currency?.id.toString()
                        : '',
                    style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
                SizedBox(width: 4),
                Text(
                  dataMainState?.balanceState is DataBalanceState
                      ? (dataMainState.balanceState as DataBalanceState).balance.toStringAsFixed(2)
                      : 'Loading...',
                  style: TextStyle(color: Colors.black),
                )
              ],
            ),
            centerTitle: true,
            background: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Current balance',
                  style: TextStyle(color: Colors.black),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
