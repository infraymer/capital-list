import 'package:capital/base/extensions.dart';
import 'package:capital/domain/account/model/account.dart';
import 'package:capital/presentation/main/bloc.dart';
import 'package:capital/presentation/main/main_state.dart';
import 'package:capital/presentation/main_appbar/bloc.dart';
import 'package:capital/presentation/main_chart/bloc.dart';
import 'package:capital/view/account/ui/account_screen.dart';
import 'package:capital/view/base/ui/styles/colors.dart';
import 'package:capital/view/base/ui/styles/wg_icons.dart';
import 'package:capital/view/injection/init_injection.dart';
import 'package:capital/view/main/main_appbar_widget.dart';
import 'package:capital/view/main/main_card_widget.dart';
import 'package:capital/view/main/ui/widget/main_chart_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainScreen extends StatefulWidget {
  static const ROUTE_NAME = '/';

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  GlobalKey<NavigatorState> _navigatorKey = getIt();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<MainBloc>(
          create: (ctx) => getIt<MainBloc>()
            ..add(FetchAccountsMainEvent()),
        ),
        BlocProvider<MainAppbarBloc>(
          create: (ctx) => getIt<MainAppbarBloc>()
            ..add(FetchMainAppbarEvent()),
        ),
        BlocProvider(
          create: (context) => getIt<MainChartBloc>()
            ..add(FetchMainChartEvent()),
        )
      ],
      child: Scaffold(
        backgroundColor: Colors.white,
        body: CustomScrollView(
          slivers: <Widget>[
            WgMainAppBar(),
            BlocBuilder<MainBloc, MainState>(
              builder: (context, state) {

                final accounts = state.cast<DataMainState>()?.accounts ?? [];

                return SliverList(
                  delegate: SliverChildListDelegate(
                    <Widget>[
                          MainChartWidget(),
                          accounts.isEmpty
                              ? _addAccountButton()
                              : _addAccountHeader(),
                          ] +
                        _accountList(accounts) +
                        [SizedBox(height: 36)]
                  ),
                );
              }
            )
          ],
        ),
      ),
    );
  }

  List<Widget> _accountList(List<Account> accounts) {
    return accounts.map((account){
      return GestureDetector(
        onTap: () => _onCardClicked(account),
        child: MainCardWidget(
          title: account.name,
          subtitle: '${account.currency.id} ${account.amount}',
          color: WgColors.accountColors[account.colorId],
        ),
      );
    }).toList();
  }

  Widget _addAccountHeader() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            'Accounts',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          IconButton(
            icon: Icon(WgFlutterApp.add),
            onPressed: _onAddAccountClicked,
          )
        ],
      ),
    );
  }

  Widget _addAccountButton() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: RaisedButton(
        elevation: 4,
        padding: EdgeInsets.only(top: 20, bottom: 24, left: 24, right: 24),
        color: WgColors.mainColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(WgFlutterApp.add, color: Colors.white),
            SizedBox(width: 20),
            Text(
              'Add your first account!',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            )
          ],
        ),
        onPressed: _onAddAccountClicked,
      ),
    );
  }

  void _onCardClicked(Account account) {
    _navigatorKey.currentState.pushNamed(AccountScreen.ROUTE_NAME, arguments: account);
  }

  void _onAddAccountClicked() {
    _navigatorKey.currentState.pushNamed(AccountScreen.ROUTE_NAME);
  }
}