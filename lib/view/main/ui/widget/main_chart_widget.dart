import 'dart:math';

import 'package:capital/base/extensions.dart';
import 'package:capital/presentation/main_chart/bloc.dart';
import 'package:capital/presentation/main_chart/model/period.dart';
import 'package:capital/view/base/ui/styles/colors.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainChartWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainChartBloc, MainChartState>(
        builder: (context, state) {
      final dataState = state.cast<DataMainChartState>();

      return Container(
        width: double.infinity,
        transform: Matrix4.identity()..translate(0.0, -40.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(40),
            bottomRight: Radius.circular(40),
          ),
          color: WgColors.mainColor,
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 60),
            Container(
              width: double.infinity,
              height: 230,
              child: dataState != null
                  ? LineChart(
                      mainData(context, dataState),
                    )
                  : Center(
                      child: CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                      ),
                    ),
            ),
            Container(
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: dataState != null
                    ? <Widget>[
                        _buildChartSelector(
                            context, '1W', Period.week, dataState.period),
                        SizedBox(width: 16),
                        _buildChartSelector(
                            context, '1M', Period.month, dataState.period),
                        SizedBox(width: 16),
                        _buildChartSelector(
                            context, '1Y', Period.year, dataState.period),
                      ]
                    : [],
              ),
            ),
          ],
        ),
      );
    });
  }

  Widget _buildChartSelector(
      BuildContext context, String name, Period period, Period selectedPeriod) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        borderRadius: BorderRadius.circular(8),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Text(
            name,
            style: TextStyle(
              color: period == selectedPeriod ? Colors.white : Colors.white60,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        onTap: () {
          BlocProvider.of<MainChartBloc>(context)
              .add(PeriodSelectedMainChartEvent(period));
        },
      ),
    );
  }

  LineChartData mainData(BuildContext context, DataMainChartState state) {
    final List<FlSpot> spots = [];
    for (var i = 0; i < state.values.length; i++) {
      spots.add(FlSpot((i + 1).toDouble(), state.values[i]));
    }

    return LineChartData(
      lineTouchData: LineTouchData(
          fullHeightTouchLine: false,
          touchTooltipData: LineTouchTooltipData(
              getTooltipItems: (List<LineBarSpot> touchedBarSpots) {
            return touchedBarSpots.map((barSpot) {
              return LineTooltipItem(
                barSpot.y.toStringAsFixed(2),
                const TextStyle(color: WgColors.mainColor),
              );
            }).toList();
          })),
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        drawHorizontalLine: false,
        getDrawingVerticalLine: (value) {
          return const FlLine(
            color: Color(0x14F9FAFE),
            strokeWidth: 2,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          textStyle: TextStyle(
            color: const Color(0x14F9FAFE),
            fontSize: 12,
          ),
          getTitles: (value) {
            if (state.period == Period.week)
              return _buildTitles1W(value);
            else if (state.period == Period.month)
              return _buildTitles1M(value);
            else
              return _buildTitles1Y(value);
          },
          margin: 8,
        ),
        leftTitles: SideTitles(showTitles: false),
      ),
      borderData: FlBorderData(show: false),
      minX: 0,
      maxX: state.values.length.toDouble() + 1,
      minY: 0,
      maxY: state.values.reduce(max) * 1.2,
      lineBarsData: [
        LineChartBarData(
          spots: spots,
          isCurved: true,
          colors: [Colors.white],
          barWidth: 4,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
        ),
      ],
    );
  }

  String _buildTitles1W(value) {
    switch (value.toInt()) {
      case 1:
        return 'mon';
      case 2:
        return 'tue';
      case 3:
        return 'wed';
      case 4:
        return 'thu';
      case 5:
        return 'fri';
      case 6:
        return 'sat';
      case 7:
        return 'sun';
    }
    return '';
  }

  String _buildTitles1M(double value) {
    switch (value.toInt()) {
      case 1:
        return '1';
      case 8:
        return '8';
      case 15:
        return '15';
      case 22:
        return '22';
      case 29:
        return '29';
    }
    return '';
  }

  String _buildTitles1Y(value) {
    switch (value.toInt()) {
      case 1:
        return '1';
      case 2:
        return '2';
      case 3:
        return '3';
      case 4:
        return '4';
      case 5:
        return '5';
      case 6:
        return '6';
      case 7:
        return '7';
      case 8:
        return '8';
      case 9:
        return '9';
      case 10:
        return '10';
      case 11:
        return '11';
      case 12:
        return '12';
    }
    return '';
  }
}
