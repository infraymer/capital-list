import 'package:capital/view/base/ui/styles/colors.dart';
import 'package:flutter/material.dart';

class MainCardWidget extends StatelessWidget {
  final Color color;
  final String title;
  final String subtitle;

  const MainCardWidget({
    Key key,
    this.title,
    this.subtitle,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      height: 100,
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 4, bottom: 3),
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.black38,
                  blurRadius: 10,
                  offset: Offset(0, 5),
                )
              ],
              borderRadius: BorderRadius.circular(10),
              color: color,
            ),
          ),
          Container(
                margin: EdgeInsets.only(right: 4, top: 4),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: WgColors.mainColor,
                ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 24),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(color: Colors.white),
                ),
                Text(
                  subtitle,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
