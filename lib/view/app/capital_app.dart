import 'package:capital/view/base/navigation/routes.dart';
import 'package:capital/view/injection/app/app_module.dart';
import 'package:capital/view/injection/init_injection.dart';
import 'package:capital/view/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';

class CapitalApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: getIt(),
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: MainScreen.ROUTE_NAME,
      onGenerateRoute: onGenerateRoute,
    );
  }
}