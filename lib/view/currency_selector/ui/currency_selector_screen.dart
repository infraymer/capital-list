import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/presentation/currency_selector/bloc.dart';
import 'package:capital/view/base/ui/styles/colors.dart';
import 'package:capital/view/base/ui/styles/shapes.dart';
import 'package:capital/view/base/ui/styles/wg_icons.dart';
import 'package:capital/view/injection/init_injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CurrencySelectorScreen extends StatefulWidget {
  static const ROUTE_NAME = '/currencySelector';
  final Currency currency;

  const CurrencySelectorScreen({Key key, this.currency}) : super(key: key);

  @override
  _CurrencySelectorScreenState createState() => _CurrencySelectorScreenState();
}

class _CurrencySelectorScreenState extends State<CurrencySelectorScreen> {

  final CurrencySelectorBloc _bloc = getIt<CurrencySelectorBloc>()
    ..add(FetchCurrencySelectorEvent());
  bool _isSearchMode = false;
  final TextEditingController _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _searchController.addListener(_onSearchValue);
  }

  void _onSearchValue() {
    final value = _searchController.text;
    _bloc.add(SearchCurrencySelectorEvent(value));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: BlocBuilder<CurrencySelectorBloc, CurrencySelectorState>(
        bloc: _bloc,
        builder: (ctx, state) {

          if (state is DataCurrencySelectorState) {
            return _buildList(state.filteredList);
          }

          if (state is LoadingCurrencySelectorState) {
            return Center(child: CircularProgressIndicator());
          }

          return SizedBox();
        },
      ),
    );
  }

  Widget _buildList(List<Currency> currencyList) {
    return ListView.separated(
      separatorBuilder: (ctx, index) {
        return Container(
          height: 1,
          width: double.infinity,
          color: WgColors.divider,
          margin: EdgeInsets.symmetric(horizontal: 16),
        );
      },
      itemCount: currencyList.length,
      itemBuilder: (ctx, index) {
        return ListTile(
          title: Text(currencyList[index].id, style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
          trailing: widget.currency == currencyList[index] ? Icon(WgFlutterApp.accepted, color: WgColors.mainColor) : null,
          onTap: () => Navigator.pop(context, currencyList[index]),
          // trailing: Icon(Icons.done),
        );
      },
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: _isSearchMode ? _buildSearchField() : Text('Choose currency'),
      backgroundColor: WgColors.mainColor,
      shape: WgShapes.appbarShape,
      actions: <Widget>[
        _isSearchMode
            ? IconButton(
          icon: Icon(Icons.clear),
          onPressed: _onClearClicked,
        )
            : IconButton(
          icon: Icon(Icons.search),
          onPressed: _onSearchClicked,
        ),
      ],
    );
  }

  Widget _buildSearchField() {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: TextField(
        autofocus: true,
        controller: _searchController,
        cursorColor: Colors.white,
        style: TextStyle(color: Colors.white),
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          disabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
      ),
    );
  }

  void _onClearClicked() {
    _searchController.clear();
  }

  void _onSearchClicked() {
    _isSearchMode = true;
    setState(() {});
  }
}
