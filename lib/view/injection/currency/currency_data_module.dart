import 'package:capital/cache/base/database.dart';
import 'package:capital/cache/currency/dao/currency_dao.dart';
import 'package:capital/cache/currency/source/currency_cache_data_source_impl.dart';
import 'package:capital/data/currency/repository/currency_repositpry_impl.dart';
import 'package:capital/data/currency/source/currency_cache_data_source.dart';
import 'package:capital/data/currency/source/currency_remote_data_source.dart';
import 'package:capital/domain/currency/repository/currency_repository.dart';
import 'package:capital/remote/currency/currency_remote_data_source_impl.dart';
import 'package:capital/view/injection/instance_names.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

GetIt currencyDataModule(GetIt getIt) {
  getIt.registerFactory<CurrencyDao>(() => getIt<AppDatabase>().currencyDao);
  getIt.registerFactory<Dio>(() => getIt.get(instanceName: InstanceNames.fixerDioClient));
  getIt.registerFactory<CurrencyRemoteDataSource>(() => CurrencyRemoteDataSourceImpl(getIt()));
  getIt.registerFactory<CurrencyCacheDataSource>(() => CurrencyCacheDataSourceImpl(getIt()));
  getIt.registerFactory<CurrencyRepository>(() => CurrencyRepositoryImpl(getIt(), getIt()));
  return getIt;
}
