import 'package:capital/domain/currency/interactor/currecncy_interactor.dart';
import 'package:capital/presentation/currency_selector/bloc.dart';
import 'package:get_it/get_it.dart';

GetIt currencyModule(GetIt getIt) {
  getIt.registerFactory(() => CurrencyInteractor(getIt()));
  getIt.registerFactory(() => CurrencySelectorBloc(getIt()));
  return getIt;
}