import 'package:capital/remote/base/api.dart';
import 'package:capital/view/injection/instance_names.dart';
import 'package:get_it/get_it.dart';

void networkModule(GetIt getIt) {
  getIt.registerSingleton(fixerDioClient, instanceName: InstanceNames.fixerDioClient);
}