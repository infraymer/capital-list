import 'package:capital/cache/base/database.dart';
import 'package:capital/cache/change_account/dao/change_account_dao.dart';
import 'package:capital/cache/change_account/source/change_account_cache_data_source.dart';
import 'package:capital/data/change_account/repository/change_account_repository_impl.dart';
import 'package:capital/data/change_account/source/change_account_cache_data_source.dart';
import 'package:capital/domain/change_account/repository/change_account_repository.dart';
import 'package:get_it/get_it.dart';

void changeAccountDataModule(GetIt getIt) {
  getIt.registerFactory<ChangeAccountDao>(() => getIt<AppDatabase>().changeAccountDao);
  getIt.registerFactory<ChangeAccountCacheDataSource>(() => ChangeAccountCacheDataSourceImpl(getIt()));
  getIt.registerFactory<ChangeAccountRepository>(() => ChangeAccountRepositoryImpl(getIt()));

}