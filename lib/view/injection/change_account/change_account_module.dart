import 'package:capital/domain/change_account/interactor/change_account_interactor.dart';
import 'package:get_it/get_it.dart';

GetIt changeAccountModule(GetIt getIt) {
  getIt.registerFactory(() => ChangeAccountInteractor(getIt()));
  return getIt;
}