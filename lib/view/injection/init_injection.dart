import 'package:capital/view/injection/account/account_data_module.dart';
import 'package:capital/view/injection/account/account_module.dart';
import 'package:capital/view/injection/app/app_module.dart';
import 'package:capital/view/injection/app/database_module.dart';
import 'package:capital/view/injection/change_account/change_account_data_module.dart';
import 'package:capital/view/injection/change_account/change_account_module.dart';
import 'package:capital/view/injection/currency/currency_data_module.dart';
import 'package:capital/view/injection/currency/currency_module.dart';
import 'package:capital/view/injection/main/main_module.dart';
import 'package:capital/view/injection/network/network_module.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

Future<void> initInjectionModules() async {
  await databaseModule(getIt);

  appModule(getIt);
  networkModule(getIt);

  mainModule(getIt);

  currencyDataModule(getIt);
  currencyModule(getIt);

  changeAccountDataModule(getIt);
  changeAccountModule(getIt);

  accountDataModule(getIt);
  accountModule(getIt);

}