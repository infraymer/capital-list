import 'package:capital/view/injection/instance_names.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';

GetIt appModule(GetIt getIt) {

  final createAccountSubject = PublishSubject<Object>();
  getIt.registerSingleton(createAccountSubject, instanceName: InstanceNames.createAccountSubject);

  getIt.registerSingleton(GlobalKey<NavigatorState>());
  return getIt;
}