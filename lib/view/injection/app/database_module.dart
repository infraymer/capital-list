import 'package:capital/cache/base/database.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<GetIt> databaseModule(GetIt getIt) async {
  final AppDatabase database = AppDatabase();
  getIt.registerSingleton(database);
  getIt.registerSingleton(SharedPreferences.getInstance());
  return getIt;
}