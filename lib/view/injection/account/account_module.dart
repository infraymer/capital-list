import 'package:capital/domain/account/interactor/account_interactor.dart';
import 'package:capital/presentation/account/account_bloc.dart';
import 'package:capital/view/injection/instance_names.dart';
import 'package:get_it/get_it.dart';

GetIt accountModule(GetIt getIt) {
  getIt.registerFactory(() => AccountInteractor(getIt()));
  getIt.registerFactory(() => AccountBloc(getIt(), getIt(), getIt.get(instanceName: InstanceNames.createAccountSubject)));
  return getIt;
}