import 'package:capital/cache/account/account_dao/account_dao.dart';
import 'package:capital/cache/account/source/account_cache_data_source_impl.dart';
import 'package:capital/cache/base/database.dart';
import 'package:capital/data/account/repository/account_repository_impl.dart';
import 'package:capital/data/account/source/account_cache_data_source.dart';
import 'package:capital/domain/account/repository/account_repository.dart';
import 'package:get_it/get_it.dart';

GetIt accountDataModule(GetIt getIt) {
  getIt.registerFactory<AccountDao>(() => getIt<AppDatabase>().accountDao);
  getIt.registerFactory<AccountCacheDataSource>(() => AccountCacheDataSourceImpl(getIt(), getIt()));
  getIt.registerFactory<AccountRepository>(() => AccountRepositoryImpl(getIt()));
  return getIt;
}
