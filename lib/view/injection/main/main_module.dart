import 'package:capital/domain/main/interactor/main_interactor.dart';
import 'package:capital/presentation/main/bloc.dart';
import 'package:capital/presentation/main_appbar/bloc.dart';
import 'package:capital/presentation/main_chart/bloc.dart';
import 'package:capital/view/injection/instance_names.dart';
import 'package:get_it/get_it.dart';

GetIt mainModule(GetIt getIt) {
  getIt.registerFactory(() => MainInteractor(getIt(), getIt(), getIt()));
  getIt.registerFactory(() => MainChartBloc(getIt(), getIt.get(instanceName: InstanceNames.createAccountSubject)));
  getIt.registerFactory(() => MainBloc(getIt(), getIt(), getIt(), getIt.get(instanceName: InstanceNames.createAccountSubject)));
  getIt.registerFactory(() => MainAppbarBloc(getIt(), getIt(), getIt.get(instanceName: InstanceNames.createAccountSubject)));
  return getIt;
}
