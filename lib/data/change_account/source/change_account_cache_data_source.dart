import 'package:capital/domain/change_account/model/change_account.dart';
import 'package:capital/domain/change_account/model/change_account_query.dart';

abstract class ChangeAccountCacheDataSource {

  Future<void> addChangeAccount(ChangeAccount changeAccount);

  Future<List<ChangeAccount>> getChangeAccountList(ChangeAccountQuery query);

  Future<ChangeAccount> getChangeAccountByDayAndByAccountId(int day, int accountId);
}