import 'package:capital/data/change_account/source/change_account_cache_data_source.dart';
import 'package:capital/domain/change_account/model/change_account.dart';
import 'package:capital/domain/change_account/model/change_account_query.dart';
import 'package:capital/domain/change_account/repository/change_account_repository.dart';

class ChangeAccountRepositoryImpl extends ChangeAccountRepository{
  final ChangeAccountCacheDataSource _accountCacheDataSource;

  ChangeAccountRepositoryImpl(this._accountCacheDataSource);

  @override
  Future<void> addChangeAccount(ChangeAccount changeAccount) async =>
      _accountCacheDataSource.addChangeAccount(changeAccount);

  @override
  Future<List<ChangeAccount>> getChangeAccountList(ChangeAccountQuery query) async =>
      _accountCacheDataSource.getChangeAccountList(query);

  @override
  Future<ChangeAccount> getChangeAccountByDayAndByAccountId(int day, int accountId) =>
      _accountCacheDataSource.getChangeAccountByDayAndByAccountId(day, accountId);
}