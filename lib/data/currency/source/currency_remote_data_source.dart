import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/domain/currency/model/rate.dart';

abstract class CurrencyRemoteDataSource {

  Future<List<Currency>> getCurrencyList();

  Future<List<Rate>> getRates(String baseCurrencyId, List<String> currencyIds);
}