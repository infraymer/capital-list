import 'package:capital/domain/currency/model/currency.dart';

abstract class CurrencyCacheDataSource {

  Future<void> setMainCurrency(Currency currency);

  Future<Currency> getMainCurrency();
}