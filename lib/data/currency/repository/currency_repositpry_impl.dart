import 'package:capital/data/currency/source/currency_cache_data_source.dart';
import 'package:capital/data/currency/source/currency_remote_data_source.dart';
import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/domain/currency/model/rate.dart';
import 'package:capital/domain/currency/repository/currency_repository.dart';

class CurrencyRepositoryImpl extends CurrencyRepository {
  final CurrencyRemoteDataSource _currencyRemoteDataSource;
  final CurrencyCacheDataSource _currencyCacheDataSource;

  CurrencyRepositoryImpl(this._currencyRemoteDataSource, this._currencyCacheDataSource);

  @override
  Future<List<Currency>> getCurrencyList() =>
      _currencyRemoteDataSource.getCurrencyList();

  @override
  Future<Currency> getMainCurrency() =>
      _currencyCacheDataSource.getMainCurrency();

  @override
  Future<void> setMainCurrency(Currency currency) =>
      _currencyCacheDataSource.setMainCurrency(currency);

  @override
  Future<List<Rate>> getRates(String baseCurrencyId, List<String> currencyIds) =>
      _currencyRemoteDataSource.getRates(baseCurrencyId, currencyIds);
}