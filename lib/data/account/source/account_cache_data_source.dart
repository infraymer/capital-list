import 'package:capital/domain/account/model/account.dart';

abstract class AccountCacheDataSource {

  Future<List<Account>> getAccounts();

  Future<void> saveAccount(Account account);

  Future<void> deleteAccount(Account account);
}