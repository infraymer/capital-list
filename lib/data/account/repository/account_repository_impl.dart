import 'package:capital/data/account/source/account_cache_data_source.dart';
import 'package:capital/domain/account/model/account.dart';
import 'package:capital/domain/account/repository/account_repository.dart';

class AccountRepositoryImpl extends AccountRepository {
  final AccountCacheDataSource _accountCacheDataSource;

  AccountRepositoryImpl(this._accountCacheDataSource);

  @override
  Future<void> saveAccount(Account account) =>
      _accountCacheDataSource.saveAccount(account);

  @override
  Future<List<Account>> getAccounts() =>
      _accountCacheDataSource.getAccounts();

  @override
  Future<void> deleteAccount(Account account) =>
      _accountCacheDataSource.deleteAccount(account);
}