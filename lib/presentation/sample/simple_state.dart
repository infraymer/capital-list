import 'package:equatable/equatable.dart';

abstract class SimpleState extends Equatable {
  const SimpleState();
  @override
  List<Object> get props => [];
}

class LoadingSimpleState extends SimpleState {}
class DataSimpleState extends SimpleState {
  final int count;
  DataSimpleState(this.count);
  @override
  List<Object> get props => [count];
}
