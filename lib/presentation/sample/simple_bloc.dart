import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:capital/base/extensions.dart';

import './bloc.dart';

class SimpleBloc extends Bloc<SimpleEvent, SimpleState> {

  @override
  SimpleState get initialState => DataSimpleState(0);

  @override
  Stream<SimpleState> mapEventToState(SimpleEvent event) async* {
    if (event is IncrementSimpleEvent) {
      yield* _mapToIncrementSimpleEvent(event);
    }
  }

  Stream<SimpleState> _mapToIncrementSimpleEvent(IncrementSimpleEvent event) async* {
    final oldState = state.cast<DataSimpleState>();
    if (oldState == null) return;
    yield LoadingSimpleState();
    await Future.delayed(Duration(seconds: 1));
    int count = oldState.count + 1;
    yield DataSimpleState(count);
  }
}
