import 'package:equatable/equatable.dart';

abstract class SimpleEvent extends Equatable {
  const SimpleEvent();
  @override
  List<Object> get props => [];
}

class IncrementSimpleEvent extends SimpleEvent{}
