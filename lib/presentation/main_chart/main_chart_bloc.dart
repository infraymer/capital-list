import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:capital/domain/main/interactor/main_interactor.dart';
import 'package:capital/presentation/main_chart/model/period.dart';
import 'package:capital/base/extensions.dart';
import './bloc.dart';

class MainChartBloc extends Bloc<MainChartEvent, MainChartState> {

  final MainInteractor _mainInteractor;
  final Stream<Object> _createAccountStream;
  StreamSubscription _createAccountSubscription;


  MainChartBloc(this._mainInteractor, this._createAccountStream) {
    _createAccountSubscription = _createAccountStream.listen((_) {
      final oldState = state.cast<DataMainChartState>();
      if (oldState == null) return;
      add(PeriodSelectedMainChartEvent(oldState.period));
    });
  }

  @override
  MainChartState get initialState => InitialMainChartState();

  @override
  Future<void> close() {
    _createAccountSubscription.cancel();
    return super.close();
  }

  @override
  Stream<MainChartState> mapEventToState(MainChartEvent event) async* {
    if (event is PeriodSelectedMainChartEvent) {
      yield* _mapPeriodSelected(event);
    } else if (event is FetchMainChartEvent) {
      yield* _mapFetch(event);
    }
  }

  Stream<MainChartState> _mapPeriodSelected(PeriodSelectedMainChartEvent event) async* {
    final oldState = state.cast<DataMainChartState>();
    if (oldState == null) return;
    try {
      yield LoadingMainChartState();
      List<double> data;
      if (event.period == Period.week)
        data = await _mainInteractor.getChartWeekValues();
      else if (event.period == Period.month)
        data = await _mainInteractor.getChartMonthValues();
      else
        data = await _mainInteractor.getChartYearValues();
      yield DataMainChartState(data, event.period);
    } catch(e) {
      yield oldState;
    }
  }

  Stream<MainChartState> _mapFetch(FetchMainChartEvent event) async* {
    final week = await _mainInteractor.getChartWeekValues();
    yield DataMainChartState(week, Period.week);
  }
}
