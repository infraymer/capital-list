import 'package:capital/presentation/main_chart/model/period.dart';
import 'package:equatable/equatable.dart';

abstract class MainChartState extends Equatable {
  const MainChartState();
  @override
  List<Object> get props => [];
}

class InitialMainChartState extends MainChartState {}
class LoadingMainChartState extends MainChartState {}
class DataMainChartState extends MainChartState {
  final List<double> values;
  final Period period;

  DataMainChartState(this.values, this.period);

  @override
  List<Object> get props => [values, period];
}