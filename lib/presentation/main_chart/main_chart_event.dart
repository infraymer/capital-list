import 'package:capital/presentation/main_chart/model/period.dart';
import 'package:equatable/equatable.dart';

abstract class MainChartEvent extends Equatable {
  const MainChartEvent();
  @override
  List<Object> get props => [];
}

class FetchMainChartEvent extends MainChartEvent {}
class PeriodSelectedMainChartEvent extends MainChartEvent {
  final Period period;
  PeriodSelectedMainChartEvent(this.period);
}