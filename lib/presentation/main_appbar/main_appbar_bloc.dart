import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:capital/domain/currency/interactor/currecncy_interactor.dart';
import 'package:capital/domain/main/interactor/main_interactor.dart';
import 'package:capital/base/extensions.dart';
import './bloc.dart';

class MainAppbarBloc extends Bloc<MainAppbarEvent, MainAppbarState> {
  final MainInteractor _mainInteractor;
  final CurrencyInteractor _currencyInteractor;
  final Stream<Object> _createAccountStream;

  StreamSubscription _createAccountSubscription;

  MainAppbarBloc(this._mainInteractor, this._currencyInteractor, this._createAccountStream) {
    _createAccountSubscription = _createAccountStream.listen((_) {
      add(FetchMainAppbarEvent());
    });
  }


  @override
  MainAppbarState get initialState => InitialMainAppbarState();

  @override
  Future<void> close() {
    _createAccountSubscription.cancel();
    return super.close();
  }

  @override
  Stream<MainAppbarState> mapEventToState(MainAppbarEvent event) async* {
    if (event is CurrencySelectedMainAppbarEvent) {
      yield* _mapToCurrencySelected(event);
    } else if (event is FetchMainAppbarEvent) {
      yield* _mapToFetchAccounts(event);
    }
  }

  Stream<MainAppbarState> _mapToFetchAccounts(FetchMainAppbarEvent event) async* {
    while(true) {
      try {
        final currency = await _currencyInteractor.getMainCurrency();
        final balance = await _mainInteractor.getBalance();
        yield DataMainAppbarState(currency: currency, balanceState: DataBalanceState(balance));
        break;
      } catch(e) {}
    }
  }

  Stream<MainAppbarState> _mapToCurrencySelected(CurrencySelectedMainAppbarEvent event) async* {
    final oldState = state.cast<DataMainAppbarState>();
    if (oldState == null) return;
    while(true) {
      try {
        await _currencyInteractor.setMainCurrency(event.currency);
        var newState = oldState.copyWith(currency: event.currency, balanceState: LoadingBalanceState());
        yield newState;
        final balance = await _mainInteractor.getBalance();
        yield newState.copyWith(balanceState: DataBalanceState(balance));
        break;
      } catch(e) {}
    }
  }
}
