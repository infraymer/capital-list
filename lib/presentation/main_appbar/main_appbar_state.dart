import 'package:capital/domain/currency/model/currency.dart';
import 'package:equatable/equatable.dart';

abstract class MainAppbarState extends Equatable {
  const MainAppbarState();
  @override
  List<Object> get props => [];
}

class InitialMainAppbarState extends MainAppbarState {}
class LoadingMainAppbarState extends MainAppbarState {}
class ErrorMainAppbarState extends MainAppbarState {}
class DataMainAppbarState extends MainAppbarState {
  final BalanceState balanceState;
  final Currency currency;

  DataMainAppbarState({this.balanceState, this.currency});

  @override
  List<Object> get props => [balanceState, currency];

  DataMainAppbarState copyWith({
    BalanceState balanceState,
    Currency currency,
  }) {
    return new DataMainAppbarState(
      balanceState: balanceState ?? this.balanceState,
      currency: currency ?? this.currency,
    );
  }
}

abstract class BalanceState extends Equatable {
  @override
  List<Object> get props => [];
}
class LoadingBalanceState extends BalanceState {}
class DataBalanceState extends BalanceState {
  final double balance;

  DataBalanceState(this.balance);
  @override
  List<Object> get props => [balance];
}
