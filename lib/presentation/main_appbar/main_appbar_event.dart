import 'package:capital/domain/currency/model/currency.dart';
import 'package:equatable/equatable.dart';

abstract class MainAppbarEvent extends Equatable {
  const MainAppbarEvent();
  @override
  List<Object> get props => [];
}

class FetchMainAppbarEvent extends MainAppbarEvent {}

class CurrencySelectedMainAppbarEvent extends MainAppbarEvent {
  final Currency currency;
  CurrencySelectedMainAppbarEvent(this.currency);
  @override
  List<Object> get props => [currency];
}
