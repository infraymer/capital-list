import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:capital/domain/account/interactor/account_interactor.dart';
import 'package:capital/domain/account/model/account.dart';
import 'package:capital/domain/change_account/interactor/change_account_interactor.dart';
import 'package:capital/domain/change_account/model/change_account.dart';
import 'package:rxdart/subjects.dart';
import 'package:uuid/uuid.dart';
import './bloc.dart';

class AccountBloc extends Bloc<AccountEvent, AccountState> {
  final AccountInteractor _accountInteractor;
  final ChangeAccountInteractor _changeAccountInteractor;
  final Subject<Object> _createAccountSubject;

  AccountBloc(this._accountInteractor, this._changeAccountInteractor, this._createAccountSubject);

  @override
  AccountState get initialState => IdleAccountState();

  @override
  Stream<AccountState> mapEventToState(AccountEvent event) async* {
    if (event is SaveAccountEvent) {
      yield* _mapToSaveAccount(event);
    } else if (event is DeleteAccountEvent) {
      yield* _mapToDeleteAccount(event);
    }
  }

  Stream<AccountState> _mapToSaveAccount(SaveAccountEvent event) async* {
    try {
      yield LoadingAccountState();
      await _accountInteractor.saveAccount(event.account);
      final changeAccount = _mapToChangeAccount(event.account, event.amountBefore);
      if (changeAccount.value != 0) {
        await _changeAccountInteractor.addChangeAccount(changeAccount);
        _createAccountSubject.add(Object());
      }
      yield SuccessAccountState();
    } catch(e) {
      yield ErrorAccountState();
    }
  }

  Stream<AccountState> _mapToDeleteAccount(DeleteAccountEvent event) async* {
    try {
      yield LoadingAccountState();
      await _accountInteractor.deleteAccount(event.account);
      _createAccountSubject.add(Object());
      yield SuccessAccountState();
    } catch(e) {
      yield ErrorAccountState();
    }
  }

  ChangeAccount _mapToChangeAccount(Account account, double amountBefore) {
    return ChangeAccount(
      id: Uuid().v4(),
      value: account.amount - amountBefore,
      amountBefore: amountBefore,
      amountAfter: account.amount,
      time:  DateTime.now().millisecondsSinceEpoch,
      accountId: account.id
    );
  }
}
