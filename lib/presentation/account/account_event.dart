import 'package:capital/domain/account/model/account.dart';
import 'package:equatable/equatable.dart';

abstract class AccountEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class SaveAccountEvent extends AccountEvent {
  final Account account;
  final double amountBefore;

  SaveAccountEvent(this.account, this.amountBefore);
  @override
  List<Object> get props => [account];
}

class DeleteAccountEvent extends AccountEvent {
  final Account account;
  DeleteAccountEvent(this.account);
  @override
  List<Object> get props => [account];
}
