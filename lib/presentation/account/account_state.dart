import 'package:equatable/equatable.dart';

abstract class AccountState extends Equatable {
  @override
  List<Object> get props => [];
}

class IdleAccountState extends AccountState {}
class LoadingAccountState extends AccountState {}
class SuccessAccountState extends AccountState {}
class ErrorAccountState extends AccountState {}
