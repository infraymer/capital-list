import 'package:capital/domain/currency/model/currency.dart';
import 'package:equatable/equatable.dart';

abstract class CurrencySelectorState extends Equatable {
  @override
  List<Object> get props => [];
}

class LoadingCurrencySelectorState extends CurrencySelectorState {}
class ErrorCurrencySelectorState extends CurrencySelectorState {}
class DataCurrencySelectorState extends CurrencySelectorState {
  final List<Currency> currencyList;
  final List<Currency> filteredList;
  DataCurrencySelectorState({this.currencyList = const [], this.filteredList = const []});
  @override
  List<Object> get props => [currencyList, filteredList];

  DataCurrencySelectorState copyWith({
    List<Currency> currencyList,
    List<Currency> filteredList,
  }) {
    return new DataCurrencySelectorState(
      currencyList: currencyList ?? this.currencyList,
      filteredList: filteredList ?? this.filteredList,
    );
  }
}