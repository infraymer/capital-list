import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:capital/domain/currency/interactor/currecncy_interactor.dart';
import './bloc.dart';
import 'package:capital/base/extensions.dart';

class CurrencySelectorBloc extends Bloc<CurrencySelectorEvent, CurrencySelectorState> {
  final CurrencyInteractor _currencyInteractor;

  @override
  CurrencySelectorState get initialState => DataCurrencySelectorState();

  CurrencySelectorBloc(this._currencyInteractor);

  @override
  Stream<CurrencySelectorState> mapEventToState(CurrencySelectorEvent event) async* {
    if (event is FetchCurrencySelectorEvent) {
      yield* _mapToFetchList(event);
    } else if (event is SearchCurrencySelectorEvent) {
      yield* _mapToSearch(event);
    }
  }

  Stream<CurrencySelectorState> _mapToFetchList(FetchCurrencySelectorEvent event) async* {
    try {
      yield LoadingCurrencySelectorState();
      final list = await _currencyInteractor.getCurrencyList();
      yield DataCurrencySelectorState(currencyList: list, filteredList: list);
    } catch(e) {
      yield ErrorCurrencySelectorState();
    }
  }

  Stream<CurrencySelectorState> _mapToSearch(SearchCurrencySelectorEvent event) async* {
    final oldState = state.cast<DataCurrencySelectorState>();
    if (oldState == null) return;
    final filtered = oldState.currencyList.where((item) => item.id.toLowerCase().contains(event.query.toLowerCase())).toList();
    yield oldState.copyWith(filteredList: event.query.isEmpty ? oldState.currencyList : filtered);
  }
}
