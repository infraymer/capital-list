import 'package:equatable/equatable.dart';

abstract class CurrencySelectorEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchCurrencySelectorEvent extends CurrencySelectorEvent {}
class SearchCurrencySelectorEvent extends CurrencySelectorEvent {
  final String query;
  SearchCurrencySelectorEvent(this.query);
  @override
  List<Object> get props => [query];
}
