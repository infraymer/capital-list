import 'package:capital/domain/account/model/account.dart';
import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/main.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class MainState extends Equatable {
  @override
  List<Object> get props => [];
}

class IdleMainState extends MainState {}
class LoadingMainState extends MainState {}

class DataMainState extends MainState {
  final List<Account> accounts;

  DataMainState({this.accounts});

  @override
  List<Object> get props => [accounts];

  DataMainState copyWith({
    List<Account> accounts,
  }) {
    return new DataMainState(
      accounts: accounts ?? this.accounts,
    );
  }
}
