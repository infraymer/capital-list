import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:capital/domain/account/interactor/account_interactor.dart';
import 'package:capital/base/extensions.dart';
import 'package:capital/domain/currency/interactor/currecncy_interactor.dart';
import 'package:capital/domain/main/interactor/main_interactor.dart';

import './bloc.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final MainInteractor _mainInteractor;
  final AccountInteractor _accountInteractor;
  final CurrencyInteractor _currencyInteractor;
  final Stream<Object> _createAccountStream;

  StreamSubscription _createAccountSubscription;

  @override
  MainState get initialState => DataMainState(accounts: []);

  MainBloc(
    this._mainInteractor,
    this._accountInteractor,
    this._currencyInteractor,
    this._createAccountStream,
  ) {
    _createAccountSubscription = _createAccountStream.listen((_) {
      add(FetchAccountsMainEvent());
    });
  }

  @override
  Future<void> close() async {
    _createAccountSubscription.cancel();
    return super.close();
  }

  @override
  Stream<MainState> mapEventToState(MainEvent event) async* {
    if (event is FetchAccountsMainEvent) {
      yield* _mapToFetchAccounts(event);
    }
  }

  Stream<MainState> _mapToFetchAccounts(FetchAccountsMainEvent event) async* {
    final oldState = state.cast<DataMainState>();
    if (oldState == null) return;
    try {
      final list = await _accountInteractor.getAccounts();
      yield DataMainState(accounts: list);
    } catch(e) {}
  }
}
