import 'package:capital/domain/currency/model/currency.dart';
import 'package:equatable/equatable.dart';

abstract class MainEvent extends Equatable {
  const MainEvent();
  @override
  List<Object> get props => [];
}

class FetchAccountsMainEvent extends MainEvent {}
