import 'package:capital/data/currency/source/currency_remote_data_source.dart';
import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/domain/currency/model/rate.dart';
import 'package:dio/dio.dart';

class CurrencyRemoteDataSourceImpl extends CurrencyRemoteDataSource {
  final Dio _fixerDioClient;

  CurrencyRemoteDataSourceImpl(this._fixerDioClient);

  @override
  Future<List<Currency>> getCurrencyList() async {
    final response = await _fixerDioClient.get('symbols');
    final map = response.data['symbols'] as Map;
    final list = map.entries.map((e) => Currency(id: e.key, name: e.value)).toList();
    return list;
  }

  @override
  Future<List<Rate>> getRates(String baseCurrencyId, List<String> currencyIds) async {
    final symbol = currencyIds.toString().replaceAll('[', '').replaceAll(']', '');
    final response = await _fixerDioClient.get(
      'latest',
      queryParameters: {
        'base': baseCurrencyId,
        'symbols': symbol,
      },
    );
    final map = response.data['rates'] as Map;
    final list = map.entries.map((e) => Rate(currencyId: e.key, value: e.value is int ? (e.value as int).toDouble() : e.value)).toList();
    return list;
  }
}