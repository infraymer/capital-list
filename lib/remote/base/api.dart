import 'package:capital/base/config.dart';
import 'package:dio/dio.dart';

final Dio fixerDioClient = new Dio(BaseOptions(
  baseUrl: fixerBaseUrl,
))..interceptors.add(
  InterceptorsWrapper(
    onRequest: (RequestOptions options) async {
      options.queryParameters['access_key'] =
      fixerApiKey;
      return options; //continue
    },
  ),
);