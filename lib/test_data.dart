import 'package:capital/domain/account/interactor/account_interactor.dart';
import 'package:capital/domain/account/model/account.dart';
import 'package:capital/domain/change_account/interactor/change_account_interactor.dart';
import 'package:capital/domain/change_account/model/change_account.dart';
import 'package:capital/domain/currency/model/currency.dart';
import 'package:capital/view/injection/init_injection.dart';

Future<void> createAccounts() async {
  final accounts = [
    Account(
      id: 1,
      name: 'Sberbank',
      amount: 27000,
      colorId: 5,
      currency: Currency(id: 'RUB', name: 'Russion Federation'),
    ),
    Account(
      id: 2,
      name: 'Tinkoff',
      amount: 161,
      colorId: 2,
      currency: Currency(id: 'EUR', name: 'Europe'),
    ),
  ];

  final changes = [
    ChangeAccount(
      id: '1',
      value: 400,
      amountBefore: 0,
      amountAfter: 400,
      time: DateTime(2020, 1, 4).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '2',
      value: 30000,
      amountBefore: 400,
      amountAfter: 30400,
      time: DateTime(2020, 1, 10).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '3',
      value: -7300,
      amountBefore: 30400,
      amountAfter: 23100,
      time: DateTime(2020, 1, 29).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '4',
      value: -5000,
      amountBefore: 23100,
      amountAfter: 18100,
      time: DateTime(2020, 2, 14).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '5',
      value: 7000,
      amountBefore: 18100,
      amountAfter: 25100,
      time: DateTime(2020, 3, 3).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '6',
      value: -15100,
      amountBefore: 25100,
      amountAfter: 10000,
      time: DateTime(2020, 3, 21).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '7',
      value: -9000,
      amountBefore: 10000,
      amountAfter: 9000,
      time: DateTime(2020, 3, 22).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '8',
      value: 3000,
      amountBefore: 9000,
      amountAfter: 12000,
      time: DateTime(2020, 3, 23).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '9',
      value: -1500,
      amountBefore: 12000,
      amountAfter: 10500,
      time: DateTime(2020, 3, 24).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '10',
      value: -1500,
      amountBefore: 12000,
      amountAfter: 10500,
      time: DateTime(2020, 3, 24).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '11',
      value: -1500,
      amountBefore: 10500,
      amountAfter: 9000,
      time: DateTime(2020, 3, 25).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '12',
      value: -5000,
      amountBefore: 9000,
      amountAfter: 4000,
      time: DateTime(2020, 3, 25).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '13',
      value: 17000,
      amountBefore: 4000,
      amountAfter: 21000,
      time: DateTime(2020, 3, 26).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '14',
      value: 6000,
      amountBefore: 21000,
      amountAfter: 27000,
      time: DateTime(2020, 3, 27).millisecondsSinceEpoch,
      accountId: 1,
    ),
    ChangeAccount(
      id: '15',
      value: 49,
      amountBefore: 0,
      amountAfter: 49,
      time: DateTime(2020, 2, 21).millisecondsSinceEpoch,
      accountId: 2,
    ),
    ChangeAccount(
      id: '16',
      value: 12,
      amountBefore: 49,
      amountAfter: 61,
      time: DateTime(2020, 3, 1).millisecondsSinceEpoch,
      accountId: 2,
    ),
    ChangeAccount(
      id: '17',
      value: 100,
      amountBefore: 61,
      amountAfter: 161,
      time: DateTime(2020, 3, 17).millisecondsSinceEpoch,
      accountId: 2,
    ),
  ];

  final AccountInteractor accountInteractor = getIt();
  final ChangeAccountInteractor changeAccountInteractor = getIt();

  for (final item in accounts) {
    await accountInteractor.saveAccount(item);
  }

  for (final item in changes) {
    await changeAccountInteractor.addChangeAccount(item);
  }
}