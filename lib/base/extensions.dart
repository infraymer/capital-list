extension ObjectExt on Object {
  T cast<T>() => this is T ? this : null;
}